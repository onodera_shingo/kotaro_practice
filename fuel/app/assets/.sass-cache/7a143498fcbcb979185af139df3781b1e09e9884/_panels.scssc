3.2.10 (Media Mark)
e1db6bc82d90d62b0312c4e87a6db77d8c860c66
o:Sass::Tree::RootNode
:@template"//
// Panels
// --------------------------------------------------


// Base class
.panel {
  margin-bottom: $line-height-computed;
  background-color: $panel-bg;
  border: 1px solid transparent;
  border-radius: $panel-border-radius;
  @include box-shadow(0 1px 1px rgba(0,0,0,.05));
}

// Panel contents
.panel-body {
  padding: $panel-body-padding;
  @include clearfix();
}

// Optional heading
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  @include border-top-radius(($panel-border-radius - 1));

  > .dropdown .dropdown-toggle {
    color: inherit;
  }
}

// Within heading, strip any `h*` tag of its default margins for spacing.
.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: ceil(($font-size-base * 1.125));
  color: inherit;

  > a {
    color: inherit;
  }
}

// Optional footer (stays gray in every modifier class)
.panel-footer {
  padding: 10px 15px;
  background-color: $panel-footer-bg;
  border-top: 1px solid $panel-inner-border;
  @include border-bottom-radius(($panel-border-radius - 1));
}


// List groups in panels
//
// By default, space out list group content from panel headings to account for
// any kind of custom content between the two.

.panel {
  > .list-group {
    margin-bottom: 0;

    .list-group-item {
      border-width: 1px 0;
      border-radius: 0;
    }

    // Add border top radius for first one
    &:first-child {
      .list-group-item:first-child {
        border-top: 0;
        @include border-top-radius(($panel-border-radius - 1));
      }
    }
    // Add border bottom radius for last one
    &:last-child {
      .list-group-item:last-child {
        border-bottom: 0;
        @include border-bottom-radius(($panel-border-radius - 1));
      }
    }
  }
}
// Collapse space between when there's no additional content.
.panel-heading + .list-group {
  .list-group-item:first-child {
    border-top-width: 0;
  }
}


// Tables in panels
//
// Place a non-bordered `.table` within a panel (not within a `.panel-body`) and
// watch it go full width.

.panel {
  > .table,
  > .table-responsive > .table {
    margin-bottom: 0;
  }
  // Add border top radius for first one
  > .table:first-child,
  > .table-responsive:first-child > .table:first-child {
    @include border-top-radius(($panel-border-radius - 1));

    > thead:first-child,
    > tbody:first-child {
      > tr:first-child {
        td:first-child,
        th:first-child {
          border-top-left-radius: ($panel-border-radius - 1);
        }
        td:last-child,
        th:last-child {
          border-top-right-radius: ($panel-border-radius - 1);
        }
      }
    }
  }
  // Add border bottom radius for last one
  > .table:last-child,
  > .table-responsive:last-child > .table:last-child {
    @include border-bottom-radius(($panel-border-radius - 1));

    > tbody:last-child,
    > tfoot:last-child {
      > tr:last-child {
        td:first-child,
        th:first-child {
          border-bottom-left-radius: ($panel-border-radius - 1);
        }
        td:last-child,
        th:last-child {
          border-bottom-right-radius: ($panel-border-radius - 1);
        }
      }
    }
  }
  > .panel-body + .table,
  > .panel-body + .table-responsive {
    border-top: 1px solid $table-border-color;
  }
  > .table > tbody:first-child > tr:first-child th,
  > .table > tbody:first-child > tr:first-child td {
    border-top: 0;
  }
  > .table-bordered,
  > .table-responsive > .table-bordered {
    border: 0;
    > thead,
    > tbody,
    > tfoot {
      > tr {
        > th:first-child,
        > td:first-child {
          border-left: 0;
        }
        > th:last-child,
        > td:last-child {
          border-right: 0;
        }
      }
    }
    > thead,
    > tbody {
      > tr:first-child {
        > td,
        > th {
          border-bottom: 0;
        }
      }
    }
    > tbody,
    > tfoot {
      > tr:last-child {
        > td,
        > th {
          border-bottom: 0;
        }
      }
    }
  }
  > .table-responsive {
    border: 0;
    margin-bottom: 0;
  }
}


// Collapsable panels (aka, accordion)
//
// Wrap a series of panels in `.panel-group` to turn them into an accordion with
// the help of our collapse JavaScript plugin.

.panel-group {
  margin-bottom: $line-height-computed;

  // Tighten up margin so it's only between panels
  .panel {
    margin-bottom: 0;
    border-radius: $panel-border-radius;
    overflow: hidden; // crop contents when collapsed
    + .panel {
      margin-top: 5px;
    }
  }

  .panel-heading {
    border-bottom: 0;
    + .panel-collapse .panel-body {
      border-top: 1px solid $panel-inner-border;
    }
  }
  .panel-footer {
    border-top: 0;
    + .panel-collapse .panel-body {
      border-bottom: 1px solid $panel-inner-border;
    }
  }
}


// Contextual variations
.panel-default {
  @include panel-variant($panel-default-border, $panel-default-text, $panel-default-heading-bg, $panel-default-border);
}
.panel-primary {
  @include panel-variant($panel-primary-border, $panel-primary-text, $panel-primary-heading-bg, $panel-primary-border);
}
.panel-success {
  @include panel-variant($panel-success-border, $panel-success-text, $panel-success-heading-bg, $panel-success-border);
}
.panel-info {
  @include panel-variant($panel-info-border, $panel-info-text, $panel-info-heading-bg, $panel-info-border);
}
.panel-warning {
  @include panel-variant($panel-warning-border, $panel-warning-text, $panel-warning-heading-bg, $panel-warning-border);
}
.panel-danger {
  @include panel-variant($panel-danger-border, $panel-danger-text, $panel-danger-heading-bg, $panel-danger-border);
}
:@children[o:Sass::Tree::CommentNode
:
@type:silent;[ :@value["J/*
 * Panels
 * -------------------------------------------------- */:@options{ :
@lineio;
;	;
;[ ;["/* Base class */;@;io:Sass::Tree::RuleNode:
@tabsi ;[
o:Sass::Tree::PropNode;i :
@name["margin-bottom;[ ;o:Sass::Script::Variable	:@underscored_name"line_height_computed;"line-height-computed;@;i;@;i:@prop_syntax:newo;;i ;["background-color;[ ;o;	;"panel_bg;"panel-bg;@;i;@;i;;o;;i ;["border;[ ;o:Sass::Script::String;	:identifier;"1px solid transparent;@;@;i;;o;;i ;["border-radius;[ ;o;	;"panel_border_radius;"panel-border-radius;@;i;@;i;;o:Sass::Tree::MixinNode;"box-shadow;[ ;@:@keywords{ ;i:@splat0:
@args[o:Sass::Script::List	;[	o:Sass::Script::Number:@numerator_units[ ;i ;@:@original"0;i:@denominator_units[ o;;["px;i;@;"1px;i; [ o;;["px;i;@;"1px;i; [ o:Sass::Script::Funcall;"	rgba;@;{ ;i;0;[	o;;[ ;i ;@;"0;i; @8o;;[ ;i ;@;"0;i; @8o;;[ ;i ;@;"0;i; @8o;;[ ;f0.050000000000000003 ��;@;"	0.05;i; @8;@;i:@separator:
space;@:
@rule[".panel;i:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;([o:#Sass::Selector::SimpleSequence
:@subject0;'@W:@sourceso:Set:
@hash{ ;i;([o:Sass::Selector::Class;["
panel;'@W;i:@has_childrenTo;
;	;
;[ ;["/* Panel contents */;@;io;;i ;[o;;i ;["padding;[ ;o;	;"panel_body_padding;"panel-body-padding;@;i;@;i;;o;;"clearfix;[ ;@;{ ;i;0;[ ;@;$[".panel-body;i;%o;&;'" ;i;([o;);([o;*
;+0;'@w;,o;-;.{ ;i;([o;/;["panel-body;'@w;i;0To;
;	;
;[ ;["/* Optional heading */;@;io;;i ;[	o;;i ;["padding;[ ;o;;	;;"10px 15px;@;@;i;;o;;i ;["border-bottom;[ ;o;;	;;"1px solid transparent;@;@;i;;o;;"border-top-radius;[ ;@;{ ;i;0;[o:Sass::Script::Operation
;@:@operand2o;;[ ;i;@;"1;i; @8;i:@operator:
minus:@operand1o;	;"panel_border_radius;"panel-border-radius;@;io;;i ;[o;;i ;["
color;[ ;o;;	;;"inherit;@;@;i!;;;@;$["!> .dropdown .dropdown-toggle;i ;%o;&;'" ;i ;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i ;([o;/;["dropdown;'@�;i o;*
;+0;'@�;,o;-;.{ ;i ;([o;/;["dropdown-toggle;'@�;i ;0T;@;$[".panel-heading;i;%o;&;'" ;i;([o;);([o;*
;+0;'@�;,o;-;.{ ;i;([o;/;["panel-heading;'@�;i;0To;
;	;
;[ ;["Q/* Within heading, strip any `h*` tag of its default margins for spacing. */;@;i%o;;i ;[
o;;i ;["margin-top;[ ;o;;	;;"0;@;@;i';;o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i(;;o;;i ;["font-size;[ ;o;!;"	ceil;@;{ ;i);0;[o;1
;@;2o;;[ ;f
1.125;@;"
1.125;i); @8;i);3:
times;5o;	;"font_size_base;"font-size-base;@;i);@;i);;o;;i ;["
color;[ ;o;;	;;"inherit;@;@;i*;;o;;i ;[o;;i ;["
color;[ ;o;;	;;"inherit;@;@;i-;;;@;$["> a;i,;%o;&;'" ;i,;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i,;([o:Sass::Selector::Element	;["a;'@�;i,:@namespace0;0T;@;$[".panel-title;i&;%o;&;'" ;i&;([o;);([o;*
;+0;'@	;,o;-;.{ ;i&;([o;/;["panel-title;'@	;i&;0To;
;	;
;[ ;["?/* Optional footer (stays gray in every modifier class) */;@;i1o;;i ;[	o;;i ;["padding;[ ;o;;	;;"10px 15px;@;@;i3;;o;;i ;["background-color;[ ;o;	;"panel_footer_bg;"panel-footer-bg;@;i4;@;i4;;o;;i ;["border-top;[ ;o;	;[o;;["px;i;@;"1px;i5; [ o;	;	;;"
solid;@;i5o;	;"panel_inner_border;"panel-inner-border;@;i5;@;i5;";#;@;i5;;o;;"border-bottom-radius;[ ;@;{ ;i6;0;[o;1
;@;2o;;[ ;i;@;"1;i6; @8;i6;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;i6;@;$[".panel-footer;i2;%o;&;'" ;i2;([o;);([o;*
;+0;'@F;,o;-;.{ ;i2;([o;/;["panel-footer;'@F;i2;0To;
;	;
;[ ;["�/* List groups in panels
 *
 * By default, space out list group content from panel headings to account for
 * any kind of custom content between the two. */;@;i:o;;i ;[o;;i ;[o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;iA;;o;;i ;[o;;i ;["border-width;[ ;o;;	;;"
1px 0;@;@;iD;;o;;i ;["border-radius;[ ;o;;	;;"0;@;@;iE;;;@;$[".list-group-item;iC;%o;&;'" ;iC;([o;);([o;*
;+0;'@p;,o;-;.{ ;iC;([o;/;["list-group-item;'@p;iC;0To;
;	;
;[ ;["./* Add border top radius for first one */;@;iHo;;i ;[o;;i ;[o;;i ;["border-top;[ ;o;;	;;"0;@;@;iK;;o;;"border-top-radius;[ ;@;{ ;iL;0;[o;1
;@;2o;;[ ;i;@;"1;iL; @8;iL;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;iL;@;$["!.list-group-item:first-child;iJ;%o;&;'" ;iJ;([o;);([o;*
;+0;'@�;,o;-;.{ ;iJ;([o;/;["list-group-item;'@�;iJo:Sass::Selector::Pseudo
;["first-child;	:
class;'@�;iJ:	@arg0;0T;@;$["&:first-child;iI;%o;&;'" ;iI;([o;);([o;*
;+0;'@�;,o;-;.{ ;iI;([o:Sass::Selector::Parent;'@�;iIo;9
;["first-child;	;:;'@�;iI;;0;0To;
;	;
;[ ;["0/* Add border bottom radius for last one */;@;iOo;;i ;[o;;i ;[o;;i ;["border-bottom;[ ;o;;	;;"0;@;@;iR;;o;;"border-bottom-radius;[ ;@;{ ;iS;0;[o;1
;@;2o;;[ ;i;@;"1;iS; @8;iS;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;iS;@;$[" .list-group-item:last-child;iQ;%o;&;'" ;iQ;([o;);([o;*
;+0;'@�;,o;-;.{ ;iQ;([o;/;["list-group-item;'@�;iQo;9
;["last-child;	;:;'@�;iQ;;0;0T;@;$["&:last-child;iP;%o;&;'" ;iP;([o;);([o;*
;+0;'@�;,o;-;.{ ;iP;([o;<;'@�;iPo;9
;["last-child;	;:;'@�;iP;;0;0T;@;$["> .list-group;i@;%o;&;'" ;i@;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i@;([o;/;["list-group;'@�;i@;0T;@;$[".panel;i?;%o;&;'" ;i?;([o;);([o;*
;+0;'@;,o;-;.{ ;i?;([o;/;["
panel;'@;i?;0To;
;	;
;[ ;["E/* Collapse space between when there's no additional content. */;@;iXo;;i ;[o;;i ;[o;;i ;["border-top-width;[ ;o;;	;;"0;@;@;i[;;;@;$["!.list-group-item:first-child;iZ;%o;&;'" ;iZ;([o;);([o;*
;+0;'@;,o;-;.{ ;iZ;([o;/;["list-group-item;'@;iZo;9
;["first-child;	;:;'@;iZ;;0;0T;@;$["!.panel-heading + .list-group;iY;%o;&;'" ;iY;([o;);([o;*
;+0;'@.;,o;-;.{ ;iY;([o;/;["panel-heading;'@.;iY"+o;*
;+0;'@.;,o;-;.{ ;iY;([o;/;["list-group;'@.;iY;0To;
;	;
;[ ;["�/* Tables in panels
 *
 * Place a non-bordered `.table` within a panel (not within a `.panel-body`) and
 * watch it go full width. */;@;i`o;;i ;[o;;i ;[o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;ih;;;@;$["-> .table,
  > .table-responsive > .table;ig;%o;&;'" ;ig;([o;);([">o;*
;+0;'@R;,o;-;.{ ;ig;([o;/;["
table;'@R;igo;);([
"
">o;*
;+0;'@R;,o;-;.{ ;ig;([o;/;["table-responsive;'@R;ig">o;*
;+0;'@R;,o;-;.{ ;ig;([o;/;["
table;'@R;ig;0To;
;	;
;[ ;["./* Add border top radius for first one */;@;ijo;;i ;[o;;"border-top-radius;[ ;@;{ ;im;0;[o;1
;@;2o;;[ ;i;@;"1;im; @8;im;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;imo;;i ;[o;;i ;[o;;i ;[o;;i ;["border-top-left-radius;[ ;o;1
;@;2o;;[ ;i;@;"1;it; @8;it;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;it;@;it;;;@;$["+td:first-child,
        th:first-child;is;%o;&;'" ;is;([o;);([o;*
;+0;'@�;,o;-;.{ ;is;([o;7	;["td;'@�;is;80o;9
;["first-child;	;:;'@�;is;;0o;);(["
o;*
;+0;'@�;,o;-;.{ ;is;([o;7	;["th;'@�;is;80o;9
;["first-child;	;:;'@�;is;;0;0To;;i ;[o;;i ;["border-top-right-radius;[ ;o;1
;@;2o;;[ ;i;@;"1;ix; @8;ix;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;ix;@;ix;;;@;$[")td:last-child,
        th:last-child;iw;%o;&;'" ;iw;([o;);([o;*
;+0;'@�;,o;-;.{ ;iw;([o;7	;["td;'@�;iw;80o;9
;["last-child;	;:;'@�;iw;;0o;);(["
o;*
;+0;'@�;,o;-;.{ ;iw;([o;7	;["th;'@�;iw;80o;9
;["last-child;	;:;'@�;iw;;0;0T;@;$["> tr:first-child;iq;%o;&;'" ;iq;([o;);([">o;*
;+0;'@�;,o;-;.{ ;iq;([o;7	;["tr;'@�;iq;80o;9
;["first-child;	;:;'@�;iq;;0;0T;@;$["1> thead:first-child,
    > tbody:first-child;ip;%o;&;'" ;ip;([o;);([">o;*
;+0;'@�;,o;-;.{ ;ip;([o;7	;["
thead;'@�;ip;80o;9
;["first-child;	;:;'@�;ip;;0o;);(["
">o;*
;+0;'@�;,o;-;.{ ;ip;([o;7	;["
tbody;'@�;ip;80o;9
;["first-child;	;:;'@�;ip;;0;0T;@;$["Q> .table:first-child,
  > .table-responsive:first-child > .table:first-child;il;%o;&;'" ;il;([o;);([">o;*
;+0;'@;,o;-;.{ ;il;([o;/;["
table;'@;ilo;9
;["first-child;	;:;'@;il;;0o;);([
"
">o;*
;+0;'@;,o;-;.{ ;il;([o;/;["table-responsive;'@;ilo;9
;["first-child;	;:;'@;il;;0">o;*
;+0;'@;,o;-;.{ ;il;([o;/;["
table;'@;ilo;9
;["first-child;	;:;'@;il;;0;0To;
;	;
;[ ;["0/* Add border bottom radius for last one */;@;i}o;;i ;[o;;"border-bottom-radius;[ ;@;{ ;i{;0;[o;1
;@;2o;;[ ;i;@;"1;i{; @8;i{;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;i{o;;i ;[o;;i ;[o;;i ;[o;;i ;["border-bottom-left-radius;[ ;o;1
;@;2o;;[ ;i;@;"1;i�; @8;i�;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;i�;@;i�;;;@;$["+td:first-child,
        th:first-child;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@`;,o;-;.{ ;i�;([o;7	;["td;'@`;i�;80o;9
;["first-child;	;:;'@`;i�;;0o;);(["
o;*
;+0;'@`;,o;-;.{ ;i�;([o;7	;["th;'@`;i�;80o;9
;["first-child;	;:;'@`;i�;;0;0To;;i ;[o;;i ;["border-bottom-right-radius;[ ;o;1
;@;2o;;[ ;i;@;"1;i�; @8;i�;3;4;5o;	;"panel_border_radius;"panel-border-radius;@;i�;@;i�;;;@;$[")td:last-child,
        th:last-child;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["td;'@�;i�;80o;9
;["last-child;	;:;'@�;i�;;0o;);(["
o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["th;'@�;i�;80o;9
;["last-child;	;:;'@�;i�;;0;0T;@;$["> tr:last-child;i;%o;&;'" ;i;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i;([o;7	;["tr;'@�;i;80o;9
;["last-child;	;:;'@�;i;;0;0T;@;$["/> tbody:last-child,
    > tfoot:last-child;i~;%o;&;'" ;i~;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i~;([o;7	;["
tbody;'@�;i~;80o;9
;["last-child;	;:;'@�;i~;;0o;);(["
">o;*
;+0;'@�;,o;-;.{ ;i~;([o;7	;["
tfoot;'@�;i~;80o;9
;["last-child;	;:;'@�;i~;;0;0T;@;$["N> .table:last-child,
  > .table-responsive:last-child > .table:last-child;i;%o;&;'" ;i;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i;([o;/;["
table;'@�;io;9
;["last-child;	;:;'@�;i;;0o;);([
"
">o;*
;+0;'@�;,o;-;.{ ;i;([o;/;["table-responsive;'@�;io;9
;["last-child;	;:;'@�;i;;0">o;*
;+0;'@�;,o;-;.{ ;i;([o;/;["
table;'@�;io;9
;["last-child;	;:;'@�;i;;0;0To;;i ;[o;;i ;["border-top;[ ;o;	;[o;;["px;i;@;"1px;i�; [ o;	;	;;"
solid;@;i�o;	;"table_border_color;"table-border-color;@;i�;@;i�;";#;@;i�;;;@;$["@> .panel-body + .table,
  > .panel-body + .table-responsive;i�;%o;&;'" ;i�;([o;);([	">o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["panel-body;'@;i�"+o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["
table;'@;i�o;);([
"
">o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["panel-body;'@;i�"+o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["table-responsive;'@;i�;0To;;i ;[o;;i ;["border-top;[ ;o;;	;;"0;@;@;i�;;;@;$["i> .table > tbody:first-child > tr:first-child th,
  > .table > tbody:first-child > tr:first-child td;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@J;,o;-;.{ ;i�;([o;/;["
table;'@J;i�">o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["
tbody;'@J;i�;80o;9
;["first-child;	;:;'@J;i�;;0">o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["tr;'@J;i�;80o;9
;["first-child;	;:;'@J;i�;;0o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["th;'@J;i�;80o;);(["
">o;*
;+0;'@J;,o;-;.{ ;i�;([o;/;["
table;'@J;i�">o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["
tbody;'@J;i�;80o;9
;["first-child;	;:;'@J;i�;;0">o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["tr;'@J;i�;80o;9
;["first-child;	;:;'@J;i�;;0o;*
;+0;'@J;,o;-;.{ ;i�;([o;7	;["td;'@J;i�;80;0To;;i ;[	o;;i ;["border;[ ;o;;	;;"0;@;@;i�;;o;;i ;[o;;i ;[o;;i ;[o;;i ;["border-left;[ ;o;;	;;"0;@;@;i�;;;@;$["/> th:first-child,
        > td:first-child;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["th;'@�;i�;80o;9
;["first-child;	;:;'@�;i�;;0o;);(["
">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["td;'@�;i�;80o;9
;["first-child;	;:;'@�;i�;;0;0To;;i ;[o;;i ;["border-right;[ ;o;;	;;"0;@;@;i�;;;@;$["-> th:last-child,
        > td:last-child;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["th;'@�;i�;80o;9
;["last-child;	;:;'@�;i�;;0o;);(["
">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["td;'@�;i�;80o;9
;["last-child;	;:;'@�;i�;;0;0T;@;$["	> tr;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["tr;'@�;i�;80;0T;@;$["&> thead,
    > tbody,
    > tfoot;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@	;,o;-;.{ ;i�;([o;7	;["
thead;'@	;i�;80o;);(["
">o;*
;+0;'@	;,o;-;.{ ;i�;([o;7	;["
tbody;'@	;i�;80o;);(["
">o;*
;+0;'@	;,o;-;.{ ;i�;([o;7	;["
tfoot;'@	;i�;80;0To;;i ;[o;;i ;[o;;i ;[o;;i ;["border-bottom;[ ;o;;	;;"0;@;@;i�;;;@;$["> td,
        > th;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@:;,o;-;.{ ;i�;([o;7	;["td;'@:;i�;80o;);(["
">o;*
;+0;'@:;,o;-;.{ ;i�;([o;7	;["th;'@:;i�;80;0T;@;$["> tr:first-child;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@T;,o;-;.{ ;i�;([o;7	;["tr;'@T;i�;80o;9
;["first-child;	;:;'@T;i�;;0;0T;@;$["> thead,
    > tbody;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@f;,o;-;.{ ;i�;([o;7	;["
thead;'@f;i�;80o;);(["
">o;*
;+0;'@f;,o;-;.{ ;i�;([o;7	;["
tbody;'@f;i�;80;0To;;i ;[o;;i ;[o;;i ;[o;;i ;["border-bottom;[ ;o;;	;;"0;@;@;i�;;;@;$["> td,
        > th;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["td;'@�;i�;80o;);(["
">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["th;'@�;i�;80;0T;@;$["> tr:last-child;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["tr;'@�;i�;80o;9
;["last-child;	;:;'@�;i�;;0;0T;@;$["> tbody,
    > tfoot;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["
tbody;'@�;i�;80o;);(["
">o;*
;+0;'@�;,o;-;.{ ;i�;([o;7	;["
tfoot;'@�;i�;80;0T;@;$["?> .table-bordered,
  > .table-responsive > .table-bordered;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["table-bordered;'@�;i�o;);([
"
">o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["table-responsive;'@�;i�">o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["table-bordered;'@�;i�;0To;;i ;[o;;i ;["border;[ ;o;;	;;"0;@;@;i�;;o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i�;;;@;$["> .table-responsive;i�;%o;&;'" ;i�;([o;);([">o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["table-responsive;'@;i�;0T;@;$[".panel;ie;%o;&;'" ;ie;([o;);([o;*
;+0;'@;,o;-;.{ ;ie;([o;/;["
panel;'@;ie;0To;
;	;
;[ ;["�/* Collapsable panels (aka, accordion)
 *
 * Wrap a series of panels in `.panel-group` to turn them into an accordion with
 * the help of our collapse JavaScript plugin. */;@;i�o;;i ;[
o;;i ;["margin-bottom;[ ;o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;;o;
;	;
;[ ;["8/* Tighten up margin so it's only between panels */;@;i�o;;i ;[
o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i�;;o;;i ;["border-radius;[ ;o;	;"panel_border_radius;"panel-border-radius;@;i�;@;i�;;o;;i ;["overflow;[ ;o;;	;;"hidden;@;@;i�;;o;
;	;
;[ ;["'/* crop contents when collapsed */;@;i�o;;i ;[o;;i ;["margin-top;[ ;o;;	;;"5px;@;@;i�;;;@;$["+ .panel;i�;%o;&;'" ;i�;([o;);(["+o;*
;+0;'@Q;,o;-;.{ ;i�;([o;/;["
panel;'@Q;i�;0T;@;$[".panel;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@`;,o;-;.{ ;i�;([o;/;["
panel;'@`;i�;0To;;i ;[o;;i ;["border-bottom;[ ;o;;	;;"0;@;@;i�;;o;;i ;[o;;i ;["border-top;[ ;o;	;[o;;["px;i;@;"1px;i�; [ o;	;	;;"
solid;@;i�o;	;"panel_inner_border;"panel-inner-border;@;i�;@;i�;";#;@;i�;;;@;$[""+ .panel-collapse .panel-body;i�;%o;&;'" ;i�;([o;);(["+o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-collapse;'@�;i�o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-body;'@�;i�;0T;@;$[".panel-heading;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-heading;'@�;i�;0To;;i ;[o;;i ;["border-top;[ ;o;;	;;"0;@;@;i�;;o;;i ;[o;;i ;["border-bottom;[ ;o;	;[o;;["px;i;@;"1px;i�; [ o;	;	;;"
solid;@;i�o;	;"panel_inner_border;"panel-inner-border;@;i�;@;i�;";#;@;i�;;;@;$[""+ .panel-collapse .panel-body;i�;%o;&;'" ;i�;([o;);(["+o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-collapse;'@�;i�o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-body;'@�;i�;0T;@;$[".panel-footer;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-footer;'@�;i�;0T;@;$[".panel-group;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-group;'@�;i�;0To;
;	;
;[ ;[" /* Contextual variations */;@;i�o;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_default_border;"panel-default-border;@;i�o;	;"panel_default_text;"panel-default-text;@;i�o;	;"panel_default_heading_bg;"panel-default-heading-bg;@;i�o;	;"panel_default_border;"panel-default-border;@;i�;@;$[".panel-default;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@;,o;-;.{ ;i�;([o;/;["panel-default;'@;i�;0To;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_primary_border;"panel-primary-border;@;i�o;	;"panel_primary_text;"panel-primary-text;@;i�o;	;"panel_primary_heading_bg;"panel-primary-heading-bg;@;i�o;	;"panel_primary_border;"panel-primary-border;@;i�;@;$[".panel-primary;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@0;,o;-;.{ ;i�;([o;/;["panel-primary;'@0;i�;0To;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_success_border;"panel-success-border;@;i�o;	;"panel_success_text;"panel-success-text;@;i�o;	;"panel_success_heading_bg;"panel-success-heading-bg;@;i�o;	;"panel_success_border;"panel-success-border;@;i�;@;$[".panel-success;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@Q;,o;-;.{ ;i�;([o;/;["panel-success;'@Q;i�;0To;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_info_border;"panel-info-border;@;i�o;	;"panel_info_text;"panel-info-text;@;i�o;	;"panel_info_heading_bg;"panel-info-heading-bg;@;i�o;	;"panel_info_border;"panel-info-border;@;i�;@;$[".panel-info;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@r;,o;-;.{ ;i�;([o;/;["panel-info;'@r;i�;0To;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_warning_border;"panel-warning-border;@;i�o;	;"panel_warning_text;"panel-warning-text;@;i�o;	;"panel_warning_heading_bg;"panel-warning-heading-bg;@;i�o;	;"panel_warning_border;"panel-warning-border;@;i�;@;$[".panel-warning;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-warning;'@�;i�;0To;;i ;[o;;"panel-variant;[ ;@;{ ;i�;0;[	o;	;"panel_danger_border;"panel-danger-border;@;i�o;	;"panel_danger_text;"panel-danger-text;@;i�o;	;"panel_danger_heading_bg;"panel-danger-heading-bg;@;i�o;	;"panel_danger_border;"panel-danger-border;@;i�;@;$[".panel-danger;i�;%o;&;'" ;i�;([o;);([o;*
;+0;'@�;,o;-;.{ ;i�;([o;/;["panel-danger;'@�;i�;0T;@;i;0T
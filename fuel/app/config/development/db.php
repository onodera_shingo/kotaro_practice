<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'mongo' => array(
		'default' => array(
			'hostname'	 => 'localhost',
			'database'	 => 'practice_bbs_db',
			'username'   => '',
			'password'   => '',
		),
	),
);

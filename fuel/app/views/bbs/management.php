<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofllow">

        <!--jQueryの読み込み-->
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <!--読み込まれたら次の処理をする-->
        <script>
            //$(document),ready(function(){
            $(function() {
                //".edit_area, .delete_area"を隠しておく
                $(".edit_area, .delete_area").hide();
                //クラスである".edit"のボタンをクリックしたとき
                $(".edit").click(function() {
                    //".delete_area"を隠しておく
                    $(".delete_area").hide();
                    // this=>".edit"　, slideToggle()でON/OFF切り替えたスライドをする
                    $(this).parents(".article").children(".edit_area").slideToggle();
                });
                //クラスである".edit"のボタンをクリックしたとき
                $(".delete").click(function() {
                    //".edit_area"を隠しておく
                    $(".edit_area").hide();
                    // this=>".delete"　, slideToggle()でON/OFF切り替えたスライドをする
                    $(this).parents(".article").children(".delete_area").slideToggle();
                });
            });</script>

        <link rel="icon" href="../assets/img/favicon.ico"> 
       <!-- <?php echo html_tag('link', array( 'rel' => 'icon', 'type' => 'image/x-icon', 'href' => Asset::get_file('favicon.ico', 'img'), ) ); ?> -->
        <title>弘太郎ちゃん掲示板 | 管理画面</title>

        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
    <!-- 
        <?php echo Asset::js('bootstrap.js'); ?>
        <?php echo Asset::css(array('bootstrap.css', 'custom.css')); ?>
    -->
    </head>

    <body class="management">
        <div class="header">
            <div class="contents">
                <h1 class="text_center title">掲示板管理ページ</h1>
                <div class="btn_area">
                    <a href="../bbs/index" target="_blank" class="btn btn-block btn-default">掲示板にもどる</a>
                    <?php if(Session::get_flash('error')) : ?>
                     <div style="color: red">
                            <?php echo Session::get_flash('error') ?>
                     </div>
                <?php endif ?>
                </div>
            </div>
        </div>
        <div class="list">
            <div class="contents">

                <?php
                    //配列の最後まで繰り返す
                    foreach ($cursor as $value) {
                ?>

                    <div class="article">
                        <div class="article_over">
                            <span><?php echo $count; ?></span>
                            <span class="name">名前 : <?php echo $value['user_name'] ?></span>
                            <span class="date"><?php echo date('Y年m月d日' , $value['timestamp']); ?></span>
                            <span class="time">投稿時間 : <?php echo date('H時i分s秒' , $value['timestamp']); ?></span>
                        </div>
                        <?php
                            if($value['update_timestamp']):
                         ?>
                                <span class="date">編集時間 : <?php echo date('Y年m月d日', $value['update_timestamp']); ?></span>
                                <span class="time"><?php echo date('H時i分s秒', $value['update_timestamp']); ?></span>
                        <?php 
                            endif
                         ?>
                        <div class="article_under">
                            <p><?php echo nl2br($value['write_area']); ?></p>
                        </div>
                        <div class="article_btns">
                            <div class="edit">
                                <button type="button" class="btn btn-block btn-default">編集するん？</button>
                            </div>
                            <div class="delete">
                                <button type="button" class="btn btn-block btn-warning">消しちゃう？</button>
                            </div>
                        </div>
                        <div class="edit_area">
                            <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
                            <form name="edit_form" role="form" action="../management/update" method="post">
                                <div class="form-group">
                                    <input name="user_name" type="text" class="form-control user-name" maxlength="8" value=<?php 
                                    if(!Session::get_flash('user_name')){
                                        echo $value['user_name'];
                                     }else{
                                         echo Session::get_flash('user_name');
                                     } ?>>
                                </div>
                                <div class="form-group">
                                    <textarea name="write_area" class="form-control write-area" rows="5" cols="40" ><?php if(!Session::get_flash('write_area')) {
                                            echo $value['write_area'];
                                            }else{
                                                echo Session::get_flash('write_area'); 
                                    } ?></textarea>
                                </div>
                                <!--nameでポストを飛ばす-->
                                <input type="hidden" name="timestamp" value=<?php echo $value['timestamp']; ?>>
                                <input type="hidden" name="mongo_id" value=<?php echo $value['_id']; ?>>
                                <input type="submit" name="submit_e" class="btn btn-block btn-primary submit-e" value="編集するん？">
                            </form>
                        </div>
                        <!--methodでpostformであることを宣言　, actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ-->
                        <form class="delete_area" action="../management/del" method="post" onsubmit="return submitChk()">
                            <p class="text_center">消しちゃいますよー？いいっすか？</p>
                            <!--nameでポストを飛ばす-->
                            <input type="hidden" name="mongo_id" value=<?php echo $value['_id']; ?>>
                            <input type="submit" name="submit_d" class="btn btn-block btn-danger" value="消しちゃいましょう！">
                        </form>
                    </div>

                    <?php
                    $count++;
                }
                ?>
            </div>
        </div>
    </body>

    <script src="../assets/javascripts/bootstrap.js"></script>
    <script>
        // 削除ボタン押下時確認confirm
        function submitChk(){
            var flg = confirm("本当に削除しますか？");

            return flg;
        }
    </script>
</html>
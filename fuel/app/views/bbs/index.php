<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofllow">
        <link rel="icon" href="../assets/img/favicon.ico"> 
    <!--  
        <?php echo html_tag('link', array( 'rel' => 'icon', 'type' => 'image/x-icon', 'href' => Asset::get_file('favicon.ico', 'img'), ) ); ?>
    -->
        <title>弘太郎ちゃん掲示板</title>

        <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/custom.css">

    <!--
        <?php echo Asset::js('bootstrap.js'); ?>
        <?php echo Asset::css(array('bootstrap.css', 'custom.css')); ?>
    -->

        <script>
        </script>
    </head>

    <body class="bbs">
        <div class="top_area">
            <div class="header">
                <div class="contents">

                    <h1 class="text_center title">KOTARO's Bulletin Board System</h1>
                    <p class="text_right">Designed by MasaNAKAMUR<a href="../management/index" target="_blank">A</a></p>
                </div>
            </div>
            <div class="input">
                <div class="contents">
                    <div class="input_inner">
                        <div class="input_area">
                            <p class="text_center">なんでもござれ</p>
                            <!--
                            methodでpostformであることを宣言
                            actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ
                            -->
                            <form name = "write_form" role="form" action="../bbs/insert" method="post">
                                <!--nameでポストを飛ばす-->
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" maxlength="8" placeholder="Enter your beautiful name" value="<?php if(Session::get_flash('user_name')) echo Session::get_flash('user_name'); ?>">
                                </div>
                                <div class="form-group">
                                    <textarea name="write_area" class="form-control" rows="5" cols="40" ><?php if(Session::get_flash('write_area')) echo Session::get_flash('write_area'); ?></textarea>
                                </div>
                                <div class="input_button">
                                    <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="とーこー">
                                </div>
                            </form>
                            <!-- 名前、本文が未入力の場合エラーメッセージを表示 -->
                            <?php if(Session::get_flash('error')) : ?>
                            <div style="color: red">
                                <?php echo Session::get_flash('error') ?>
                             </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="list">
            <div class="contents">
                <?php
                foreach ($cursor as $value) {
                    ?>
                    <div class="article"><!-- このdivを繰り返し使うがいいさ！ -->
                        <div class="article_over">
                            <span><?php echo $count; ?></span>
                            <span class="name">名前 : <?php echo $value['user_name']; ?></span>
                         <span class="date"><?php echo date('Y年m月d日', $value['timestamp']); ?></span> 
                            <span class="time">投稿時間 : <?php echo date('H時i分s秒', $value['timestamp']); ?></span>
                        </div>
                        <?php 
                            if($value['update_timestamp']):
                         ?>
                                <span class="date">編集時間 : <?php echo date('Y年m月d日', $value['update_timestamp']); ?></span>
                                <span class="time"><?php echo date('H時i分s秒', $value['update_timestamp']); ?></span>
                        <?php
                            endif
                         ?>
                        <div class="article_under">
                            <p><?php echo nl2br($value['write_area']); ?></p>
                        </div>
                    </div>
                    <?php
                    $count++;
                }
                ?>
            </div>
        </div>

    </body>
    <script src="../../../assets/javascripts/bootstrap.js"></script>
</html>
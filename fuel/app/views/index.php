<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofllow">

        <script>
            //入力チェックの関数
            function check() {
                //nameを指定し、valueの値がからでないかを確認
                if (document.write_form.user_name.value === "" && document.write_form.write_area.value === "") {
                    //アラート
                    alert("名前と本文を入力してください");
                    // 送信中止
                    return false;
                } else if (document.write_form.user_name.value === "") {
                    alert("名前を入力してください");
                    return false;
                } else if (document.write_form.write_area.value === "") {
                    alert("本文を入力してください");
                    return false;
                }
            }
        </script>

        <link rel="icon" href="../assets/img/favicon.ico">
        <title>弘太郎ちゃん掲示板</title>

        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/custom.css">
    </head>

    <body class="bbs">
        <?php
        //接続
        //Mongo
        $mongo = new MongoClient();
        //データベース
        $db = $mongo->selectDB('practice_bbs_db');
        //コレクション
        $collection = $db->selectCollection('bbs');

        //POSTフラグ
        $postflg = '';
        //SERVERのリクエストがPOSTの場合
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //ポストされたときのフラグ
            $postflg = '1';
            //<input>のnameであるsubmitがポストされているかどうかの判定
            if (isset($_POST['submit'])) {
                //$postflg = "1";
                //textとtextareaの値取得
                $user_name = htmlspecialchars($_POST['user_name'], ENT_QUOTES);
                $write_area = htmlspecialchars($_POST['write_area'], ENT_QUOTES);
            }
        }

        if ($postflg === '1') {
            //インサート
            $collection->insert(array('user_name' => $user_name, 'write_area' => $write_area, 'timestamp' => time()));
            // 二重投稿対策のためのheader関数
            header("Location: {$_SERVER['PHP_SELF']}");
            exit;
        }

        //foreachの処理宣言
        //collectionの行を検索
        $cursor = $collection->find()->sort(array('timestamp' => -1));
        //var_dump($cursor);
        date_default_timezone_set('Asia/Tokyo');
        $count = 1;
        ?>
        <div class="top_area">
            <div class="header">
                <div class="contents">

                    <h1 class="text_center title">KOTARO's Bulletin Board System</h1>
                    <p class="text_right">Designed by MasaNAKAMUR<a href="./management.php" target="_blank">A</a></p>
                </div>
            </div>
            <div class="input">
                <div class="contents">
                    <div class="input_inner">
                        <div class="input_area">
                            <p class="text_center">なんでもござれ</p>
                            <!--methodでpostformであることを宣言
                            actionでどこにpostの値を投げるかを指定。action=””だと自分のソースファイルの飛ぶ
                            onsubmitでreturnがfalseの場合送信を中止-->
                            <form name = "write_form" role="form" action="" method="post"  onsubmit="return check()">
                                <!--nameでポストを飛ばす-->
                                <div class="form-group">
                                    <input type="text" name="user_name" class="form-control" maxlength="8" placeholder="Enter your beautiful name">
                                </div>
                                <div class="form-group">
                                    <textarea name="write_area" class="form-control" rows="5" cols="40"></textarea>
                                </div>
                                <div class="input_button">
                                    <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="とーこー">
                                </div>
                            </form>
                        </div>
                        <!--
                        <div class="input_button">
                                <button type="button" class="btn btn-primary btn-lg btn-block">かきこむ</button>
                        </div>
                        -->
                    </div>
                </div>
            </div>
        </div>

        <div class="list">
            <div class="contents">
                <?php
                foreach ($cursor as $value) {
                    ?>
                    <div class="article"><!-- このdivを繰り返し使うがいいさ！ -->
                        <div class="article_over">
                            <span><?php echo $count; ?></span>
                            <span class="name">名前 : <?php echo $value['user_name']; ?></span>
                            <span class="date"><?php echo date('Y年m月d日', $value['timestamp']); ?></span>
                            <span class="time">投稿時間 : <?php echo date('H時i分s秒', $value['timestamp']); ?></span>
                        </div>
                        <div class="article_under">
                            <p><?php echo $value['write_area']; ?></p>
                        </div>
                    </div>

                    <?php
                    $count++;
                }
                ?>

                <!--
                <div class="article"><!-- このdivを繰り返し使うがいいさ！ 
                    <div class="article_over">
                        <span>001</span>
                        <span class="name">名前 : </span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 10:50:34</span>
                    </div>
                    <div class="article_under">
                        <p>あーー。まじ北上は酒の売れ行きパねえ。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>002</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">編集時間 : 11:53:24</span>
                    </div>
                    <div class="article_under">
                        <p>おーたか、おやじ車買ってたぞ。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>003</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:03:10</span>
                    </div>
                    <div class="article_under">
                        <p>あーー？まじか？腹立つな。こうなったら塩なめながら酒飲むしかねえ。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>004</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:22:19</span>
                    </div>
                    <div class="article_under">
                        <p>ホットケーキでも作るか？</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>005</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:25:17</span>
                    </div>
                    <div class="article_under">
                        <p>兄貴のホットケーキはマジでうまい。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>006</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:27:19</span>
                    </div>
                    <div class="article_under">
                        <p>ホットケーキでも作るか？</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>007</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:28:17</span>
                    </div>
                    <div class="article_under">
                        <p>兄貴のホットケーキはマジでうまい。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>008</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:29:19</span>
                    </div>
                    <div class="article_under">
                        <p>ホットケーキでも作るか？</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>009</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:30:17</span>
                    </div>
                    <div class="article_under">
                        <p>兄貴のホットケーキはマジでうまい。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>010</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:31:19</span>
                    </div>
                    <div class="article_under">
                        <p>ホットケーキでも作るか？</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>011</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:32:17</span>
                    </div>
                    <div class="article_under">
                        <p>マジでうまい。</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>012</span>
                        <span class="name">名前 : 佐藤一明</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:32:19</span>
                    </div>
                    <div class="article_under">
                        <p>ホットケーキでも作るか？</p>
                    </div>
                </div>
                <div class="article">
                    <div class="article_over">
                        <span>013</span>
                        <span class="name">名前 : 佐藤孝則</span>
                        <span class="date">2014/05/09(金)</span>
                        <span class="time">投稿時間 : 12:32:22</span>
                    </div>
                    <div class="article_under">
                        <p>マジで。</p>
                    </div>
                </div>
                -->
            </div>
        </div>

    </body>

    <script src="../assets/javascripts/bootstrap.js"></script>

</html>
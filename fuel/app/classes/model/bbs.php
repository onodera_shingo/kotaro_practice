<?php
use \Model_Crud;
class Model_Bbs extends Model_Crud
{	

	// 検索メソッド
	public static function findBbs()
	{	
		$mongodb = \Mongo_Db::instance();
		return $mongodb->order_by(['timestamp' => -1])
					   ->get('bbs');
	}

	// 登録メソッド
	public static function insert($data)
	{	
		$mongodb = \Mongo_Db::instance();
		$mongodb->insert('bbs' , $data);
	}

	// 更新メソッド
	public static function update($data)
	{
		$mongodb = \Mongo_Db::instance();
		$mongodb->where(['_id' => new \MongoId($data['_id'])])
			    ->update('bbs' , 
					[
						'user_name' 	   => $data['user_name'],
						'write_area' 	   => $data['write_area'],
						'update_timestamp' => time(),
					]			
		);
	}

	// 削除メソッド
	public static function del($_id)
	{
		$mongodb = \Mongo_Db::instance();
		return $mongodb->where(['_id' => new \MongoId($_id)])
					   ->delete('bbs');
	}

	// Valdateメソッド
	public static function validate($data)
	{
		// Validationオブジェクト生成
		$val = Validation::forge();
		$val->add_field('user_name' , '名前' , 'required');
		$val->add_field('write_area' , '本文' , 'required');
		$val->set_message('required' , ':labelが入力されていません。');

		$out = '';

		if($val->run()){
			$out = 'true';// 成功時
		}else{
			foreach($val->error() as $error){
				$out .= $error . '<br>';// 失敗時エラーメッセージを登録
			}
		}
		return $out;
	}
}
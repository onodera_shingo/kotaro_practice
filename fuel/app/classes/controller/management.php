<?php
class Controller_Management extends Controller_Rest
{
	public static function action_index()
	{	
		// 掲示板データ初期化
		$data = array();
		// モデルから掲示板データを取得
		$data['cursor'] = Model_Bbs::findBbs();
		$data['count']  = 1;

		return Response::forge(View::forge('bbs/management' , $data));
	}

	// 更新処理
	public static function action_update()
	{
		try{
			// 入力パラメータ取得
			$_id 		= htmlspecialchars($_POST['mongo_id'] , ENT_QUOTES,'utf-8'); // 掲示板ID
			$user_name  = htmlspecialchars($_POST['user_name'] , ENT_QUOTES,'utf-8'); // 名前
			$write_area = htmlspecialchars($_POST['write_area'] , ENT_QUOTES,'utf-8'); // 本文

			// 入力パラメータValidation
			$result = Model_Bbs::Validate([
				'user_name'  => $user_name,
			 	'write_area' => $write_area,
			 	]
			);
			// ValidationOK
			if($result === 'true'){
				// 更新処理
				Model_Bbs::update([
					'_id' 			   => $_id,
					'user_name' 	   => $user_name,
					'write_area' 	   => $write_area,
					'update_timestamp' => time(),
					]
				);
			// ValidationNG
			}else{
				// エラーメッセージ、入力パラメータをSessionにセット
				Session::set_flash('error' , $result);
				Session::set_flash('user_name' , $user_name);
				Session::set_flash('write_area' , $write_area);
			}
		}catch(Exception $e){
			// Exception発生時エラーメッセージ、入力パラメータを取得しSessionにセット
			Session::set_flash('error' , $e->getmessage());
			Session::set_flash('user_name' , $user_name);
			Session::set_flash('write_area' , $write_area);
		}
		Response::redirect('management/index');
	}

	// 削除処理
	public static function action_del()
	{
		try{
			// ID取得
			$_id = htmlspecialchars($_POST['mongo_id'] , ENT_QUOTES,'utf-8');
			// 削除実行
			Model_Bbs::del($_id);
		} catch (Exception $e){
			// Exception発生時エラーメッセージをSessionにセット
			Session::set_flash('error' , $e->getmessage());
		}
		Response::redirect('management/index');
	}	

	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404') , 404);
	}
}

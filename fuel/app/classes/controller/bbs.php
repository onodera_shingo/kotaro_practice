<?php

class Controller_Bbs extends Controller
{
	// 検索処理
	public function action_index()
	{
		// 掲示板データ初期化
		$data = array();
		// モデルから掲示板データを取得
		$data['cursor'] = Model_Bbs::findBbs();
		$data['count']  = 1;

		return Response::forge(View::forge('bbs/index',$data));
	}
	// 登録処理
	public function action_insert()
	{	
		try{
			// 入力パラメータ
			$user_name  = htmlspecialchars($_POST['user_name'] , ENT_QUOTES,'utf-8');
			$write_area = htmlspecialchars($_POST['write_area'] , ENT_QUOTES,'utf-8');
			// 入力パラメータValidate
			$result = Model_Bbs::validate([
					'user_name' => $user_name,
					'write_area'=> $write_area,
					]
				);
			// ValidateOK
			if($result === 'true')
			{
				Model_Bbs::insert([
					'user_name'  	   => $user_name,
					'write_area' 	   => $write_area,
					'timestamp'  	   => time(),
					'update_timestamp' => null,
					]
				);
			// ValidateNG
			}else{
				Session::set_flash('error' , $result);
				Session::set_flash('user_name' , $user_name);
				Session::set_flash('write_area' , $write_area);	
			}
		}catch(Exception $e){
			// Exception発生時エラーメッセージ、入力パラメータを取得
			Session::set_flash('error' , $e->getmessage());
			Session::set_flash('user_name' , $user_name);
			Session::set_flash('write_area' , $write_area);

		}

		Response::redirect('bbs/index');
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}

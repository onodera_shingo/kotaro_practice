3.2.10 (Media Mark)
876549c002d120e4f49f30c56426880f786b585e
o:Sass::Tree::RootNode
:@template"V//
// Navs
// --------------------------------------------------


// Base class
// --------------------------------------------------

.nav {
  margin-bottom: 0;
  padding-left: 0; // Override default ul/ol
  list-style: none;
  @include clearfix();

  > li {
    position: relative;
    display: block;

    > a {
      position: relative;
      display: block;
      padding: $nav-link-padding;
      &:hover,
      &:focus {
        text-decoration: none;
        background-color: $nav-link-hover-bg;
      }
    }

    // Disabled state sets text to gray and nukes hover/tab effects
    &.disabled > a {
      color: $nav-disabled-link-color;

      &:hover,
      &:focus {
        color: $nav-disabled-link-hover-color;
        text-decoration: none;
        background-color: transparent;
        cursor: not-allowed;
      }
    }
  }

  // Open dropdowns
  .open > a {
    &,
    &:hover,
    &:focus {
      background-color: $nav-link-hover-bg;
      border-color: $link-color;
    }
  }

  // Nav dividers (deprecated with v3.0.1)
  //
  // This should have been removed in v3 with the dropping of `.nav-list`, but
  // we missed it. We don't currently support this anywhere, but in the interest
  // of maintaining backward compatibility in case you use it, it's deprecated.
  .nav-divider {
    @include nav-divider();
  }

  // Prevent IE8 from misplacing imgs
  //
  // See https://github.com/h5bp/html5-boilerplate/issues/984#issuecomment-3985989
  > li > a > img {
    max-width: none;
  }
}


// Tabs
// -------------------------

// Give the tabs something to sit on
.nav-tabs {
  border-bottom: 1px solid $nav-tabs-border-color;
  > li {
    float: left;
    // Make the list-items overlay the bottom border
    margin-bottom: -1px;

    // Actual tabs (as links)
    > a {
      margin-right: 2px;
      line-height: $line-height-base;
      border: 1px solid transparent;
      border-radius: $border-radius-base $border-radius-base 0 0;
      &:hover {
        border-color: $nav-tabs-link-hover-border-color $nav-tabs-link-hover-border-color $nav-tabs-border-color;
      }
    }

    // Active state, and its :hover to override normal :hover
    &.active > a {
      &,
      &:hover,
      &:focus {
        color: $nav-tabs-active-link-hover-color;
        background-color: $nav-tabs-active-link-hover-bg;
        border: 1px solid $nav-tabs-active-link-hover-border-color;
        border-bottom-color: transparent;
        cursor: default;
      }
    }
  }
  // pulling this in mainly for less shorthand
  &.nav-justified {
    @extend .nav-justified;
    @extend .nav-tabs-justified;
  }
}


// Pills
// -------------------------
.nav-pills {
  > li {
    float: left;

    // Links rendered as pills
    > a {
      border-radius: $nav-pills-border-radius;
    }
    + li {
      margin-left: 2px;
    }

    // Active state
    &.active > a {
      &,
      &:hover,
      &:focus {
        color: $nav-pills-active-link-hover-color;
        background-color: $nav-pills-active-link-hover-bg;
      }
    }
  }
}


// Stacked pills
.nav-stacked {
  > li {
    float: none;
    + li {
      margin-top: 2px;
      margin-left: 0; // no need for this gap between nav items
    }
  }
}


// Nav variations
// --------------------------------------------------

// Justified nav links
// -------------------------

.nav-justified {
  width: 100%;

  > li {
    float: none;
     > a {
      text-align: center;
      margin-bottom: 5px;
    }
  }

  > .dropdown .dropdown-menu {
    top: auto;
    left: auto;
  }

  @media (min-width: $screen-sm-min) {
    > li {
      display: table-cell;
      width: 1%;
      > a {
        margin-bottom: 0;
      }
    }
  }
}

// Move borders to anchors instead of bottom of list
//
// Mixin for adding on top the shared `.nav-justified` styles for our tabs
.nav-tabs-justified {
  border-bottom: 0;

  > li > a {
    // Override margin from .nav-tabs
    margin-right: 0;
    border-radius: $border-radius-base;
  }

  > .active > a,
  > .active > a:hover,
  > .active > a:focus {
    border: 1px solid $nav-tabs-justified-link-border-color;
  }

  @media (min-width: $screen-sm-min) {
    > li > a {
      border-bottom: 1px solid $nav-tabs-justified-link-border-color;
      border-radius: $border-radius-base $border-radius-base 0 0;
    }
    > .active > a,
    > .active > a:hover,
    > .active > a:focus {
      border-bottom-color: $nav-tabs-justified-active-link-border-color;
    }
  }
}


// Tabbable tabs
// -------------------------

// Hide tabbable panes to start, show them when `.active`
.tab-content {
  > .tab-pane {
    display: none;
  }
  > .active {
    display: block;
  }
}


// Dropdowns
// -------------------------

// Specific dropdowns
.nav-tabs .dropdown-menu {
  // make dropdown border overlap tab border
  margin-top: -1px;
  // Remove the top rounded corners here since there is a hard edge above the menu
  @include border-top-radius(0);
}
:@children[o:Sass::Tree::CommentNode
:
@type:silent;[ :@value["H/*
 * Navs
 * -------------------------------------------------- */:@options{ :
@lineio;
;	;
;[ ;["K/* Base class
 * -------------------------------------------------- */;@;io:Sass::Tree::RuleNode:
@tabsi ;[o:Sass::Tree::PropNode;i :
@name["margin-bottom;[ ;o:Sass::Script::String;	:identifier;"0;@;@;i:@prop_syntax:newo;;i ;["padding-left;[ ;o;;	;;"0;@;@;i;;o;
;	;
;[ ;["!/* Override default ul/ol */;@;io;;i ;["list-style;[ ;o;;	;;"	none;@;@;i;;o:Sass::Tree::MixinNode;"clearfix;[ ;@:@keywords{ ;i:@splat0:
@args[ o;;i ;[
o;;i ;["position;[ ;o;;	;;"relative;@;@;i;;o;;i ;["display;[ ;o;;	;;"
block;@;@;i;;o;;i ;[	o;;i ;["position;[ ;o;;	;;"relative;@;@;i;;o;;i ;["display;[ ;o;;	;;"
block;@;@;i;;o;;i ;["padding;[ ;o:Sass::Script::Variable	:@underscored_name"nav_link_padding;"nav-link-padding;@;i;@;i;;o;;i ;[o;;i ;["text-decoration;[ ;o;;	;;"	none;@;@;i;;o;;i ;["background-color;[ ;o;	;"nav_link_hover_bg;"nav-link-hover-bg;@;i;@;i;;;@:
@rule["&:hover,
      &:focus;i:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence; [o:#Sass::Selector::SimpleSequence
:@subject0;@c:@sourceso:Set:
@hash{ ;i; [o:Sass::Selector::Parent;@c;io:Sass::Selector::Pseudo
;["
hover;	:
class;@c;i:	@arg0o;!; ["
o;"
;#0;@c;$o;%;&{ ;i; [o;';@c;io;(
;["
focus;	;);@c;i;*0:@has_childrenT;@;["> a;i;o;;" ;i; [o;!; [">o;"
;#0;@};$o;%;&{ ;i; [o:Sass::Selector::Element	;["a;@};i:@namespace0;+To;
;	;
;[ ;["G/* Disabled state sets text to gray and nukes hover/tab effects */;@;i#o;;i ;[o;;i ;["
color;[ ;o;	;"nav_disabled_link_color;"nav-disabled-link-color;@;i%;@;i%;;o;;i ;[	o;;i ;["
color;[ ;o;	;""nav_disabled_link_hover_color;""nav-disabled-link-hover-color;@;i);@;i);;o;;i ;["text-decoration;[ ;o;;	;;"	none;@;@;i*;;o;;i ;["background-color;[ ;o;;	;;"transparent;@;@;i+;;o;;i ;["cursor;[ ;o;;	;;"not-allowed;@;@;i,;;;@;["&:hover,
      &:focus;i(;o;;" ;i(; [o;!; [o;"
;#0;@�;$o;%;&{ ;i(; [o;';@�;i(o;(
;["
hover;	;);@�;i(;*0o;!; ["
o;"
;#0;@�;$o;%;&{ ;i(; [o;';@�;i(o;(
;["
focus;	;);@�;i(;*0;+T;@;["&.disabled > a;i$;o;;" ;i$; [o;!; [o;"
;#0;@�;$o;%;&{ ;i$; [o;';@�;i$o:Sass::Selector::Class;["disabled;@�;i$">o;"
;#0;@�;$o;%;&{ ;i$; [o;,	;["a;@�;i$;-0;+T;@;["	> li;i;o;;" ;i; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i; [o;,	;["li;@�;i;-0;+To;
;	;
;[ ;["/* Open dropdowns */;@;i1o;;i ;[o;;i ;[o;;i ;["background-color;[ ;o;	;"nav_link_hover_bg;"nav-link-hover-bg;@;i6;@;i6;;o;;i ;["border-color;[ ;o;	;"link_color;"link-color;@;i7;@;i7;;;@;[" &,
    &:hover,
    &:focus;i5;o;;" ;i5; [o;!; [o;"
;#0;@;$o;%;&{ ;i5; [o;';@;i5o;!; ["
o;"
;#0;@;$o;%;&{ ;i5; [o;';@;i5o;(
;["
hover;	;);@;i5;*0o;!; ["
o;"
;#0;@;$o;%;&{ ;i5; [o;';@;i5o;(
;["
focus;	;);@;i5;*0;+T;@;[".open > a;i2;o;;" ;i2; [o;!; [o;"
;#0;@';$o;%;&{ ;i2; [o;.;["	open;@';i2">o;"
;#0;@';$o;%;&{ ;i2; [o;,	;["a;@';i2;-0;+To;
;	;
;[ ;["/* Nav dividers (deprecated with v3.0.1)
 *
 * This should have been removed in v3 with the dropping of `.nav-list`, but
 * we missed it. We don't currently support this anywhere, but in the interest
 * of maintaining backward compatibility in case you use it, it's deprecated. */;@;i;o;;i ;[o;;"nav-divider;[ ;@;{ ;iA;0;[ ;@;[".nav-divider;i@;o;;" ;i@; [o;!; [o;"
;#0;@H;$o;%;&{ ;i@; [o;.;["nav-divider;@H;i@;+To;
;	;
;[ ;["/* Prevent IE8 from misplacing imgs
 *
 * See https://github.com/h5bp/html5-boilerplate/issues/984#issuecomment-3985989 */;@;iDo;;i ;[o;;i ;["max-width;[ ;o;;	;;"	none;@;@;iH;;;@;["> li > a > img;iG;o;;" ;iG; [o;!; [">o;"
;#0;@b;$o;%;&{ ;iG; [o;,	;["li;@b;iG;-0">o;"
;#0;@b;$o;%;&{ ;iG; [o;,	;["a;@b;iG;-0">o;"
;#0;@b;$o;%;&{ ;iG; [o;,	;["img;@b;iG;-0;+T;@;["	.nav;i;o;;" ;i; [o;!; [o;"
;#0;@�;$o;%;&{ ;i; [o;.;["nav;@�;i;+To;
;	;
;[ ;[",/* Tabs
 * ------------------------- */;@;iMo;
;	;
;[ ;[",/* Give the tabs something to sit on */;@;iPo;;i ;[	o;;i ;["border-bottom;[ ;o:Sass::Script::List	;[o:Sass::Script::Number:@numerator_units["px;i;@:@original"1px;iR:@denominator_units[ o;	;	;;"
solid;@;iRo;	;"nav_tabs_border_color;"nav-tabs-border-color;@;iR;@;iR:@separator:
space;@;iR;;o;;i ;[o;;i ;["
float;[ ;o;;	;;"	left;@;@;iT;;o;
;	;
;[ ;["8/* Make the list-items overlay the bottom border */;@;iUo;;i ;["margin-bottom;[ ;o;;	;;"	-1px;@;@;iV;;o;
;	;
;[ ;["!/* Actual tabs (as links) */;@;iXo;;i ;[
o;;i ;["margin-right;[ ;o;;	;;"2px;@;@;iZ;;o;;i ;["line-height;[ ;o;	;"line_height_base;"line-height-base;@;i[;@;i[;;o;;i ;["border;[ ;o;;	;;"1px solid transparent;@;@;i\;;o;;i ;["border-radius;[ ;o;/	;[	o;	;"border_radius_base;"border-radius-base;@;i]o;	;"border_radius_base;"border-radius-base;@;i]o;0;1[ ;i ;@;2"0;i];3[ o;0;1[ ;i ;@;2"0;i];3@�;@;i];4;5;@;i];;o;;i ;[o;;i ;["border-color;[ ;o;/	;[o;	;"%nav_tabs_link_hover_border_color;"%nav-tabs-link-hover-border-color;@;i_o;	;"%nav_tabs_link_hover_border_color;"%nav-tabs-link-hover-border-color;@;i_o;	;"nav_tabs_border_color;"nav-tabs-border-color;@;i_;@;i_;4;5;@;i_;;;@;["&:hover;i^;o;;" ;i^; [o;!; [o;"
;#0;@�;$o;%;&{ ;i^; [o;';@�;i^o;(
;["
hover;	;);@�;i^;*0;+T;@;["> a;iY;o;;" ;iY; [o;!; [">o;"
;#0;@;$o;%;&{ ;iY; [o;,	;["a;@;iY;-0;+To;
;	;
;[ ;["A/* Active state, and its :hover to override normal :hover */;@;ico;;i ;[o;;i ;[
o;;i ;["
color;[ ;o;	;"%nav_tabs_active_link_hover_color;"%nav-tabs-active-link-hover-color;@;ih;@;ih;;o;;i ;["background-color;[ ;o;	;""nav_tabs_active_link_hover_bg;""nav-tabs-active-link-hover-bg;@;ii;@;ii;;o;;i ;["border;[ ;o;/	;[o;0;1["px;i;@;2"1px;ij;3[ o;	;	;;"
solid;@;ijo;	;",nav_tabs_active_link_hover_border_color;",nav-tabs-active-link-hover-border-color;@;ij;@;ij;4;5;@;ij;;o;;i ;["border-bottom-color;[ ;o;;	;;"transparent;@;@;ik;;o;;i ;["cursor;[ ;o;;	;;"default;@;@;il;;;@;["$&,
      &:hover,
      &:focus;ig;o;;" ;ig; [o;!; [o;"
;#0;@H;$o;%;&{ ;ig; [o;';@H;igo;!; ["
o;"
;#0;@H;$o;%;&{ ;ig; [o;';@H;igo;(
;["
hover;	;);@H;ig;*0o;!; ["
o;"
;#0;@H;$o;%;&{ ;ig; [o;';@H;igo;(
;["
focus;	;);@H;ig;*0;+T;@;["&.active > a;id;o;;" ;id; [o;!; [o;"
;#0;@j;$o;%;&{ ;id; [o;';@j;ido;.;["active;@j;id">o;"
;#0;@j;$o;%;&{ ;id; [o;,	;["a;@j;id;-0;+T;@;["	> li;iS;o;;" ;iS; [o;!; [">o;"
;#0;@�;$o;%;&{ ;iS; [o;,	;["li;@�;iS;-0;+To;
;	;
;[ ;["4/* pulling this in mainly for less shorthand */;@;ipo;;i ;[o:Sass::Tree::ExtendNode
;[ ;@:@selector["."nav-justified;ir:@optionalFo;6
;[ ;@;7["."nav-tabs-justified;is;8F;@;["&.nav-justified;iq;o;;" ;iq; [o;!; [o;"
;#0;@�;$o;%;&{ ;iq; [o;';@�;iqo;.;["nav-justified;@�;iq;+T;@;[".nav-tabs;iQ;o;;" ;iQ; [o;!; [o;"
;#0;@�;$o;%;&{ ;iQ; [o;.;["nav-tabs;@�;iQ;+To;
;	;
;[ ;["-/* Pills
 * ------------------------- */;@;ixo;;i ;[o;;i ;[o;;i ;["
float;[ ;o;;	;;"	left;@;@;i|;;o;
;	;
;[ ;[""/* Links rendered as pills */;@;i~o;;i ;[o;;i ;["border-radius;[ ;o;	;"nav_pills_border_radius;"nav-pills-border-radius;@;i{;@;i{;;;@;["> a;i;o;;" ;i; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i; [o;,	;["a;@�;i;-0;+To;;i ;[o;;i ;["margin-left;[ ;o;;	;;"2px;@;@;i~;;;@;["	+ li;i};o;;" ;i}; [o;!; ["+o;"
;#0;@�;$o;%;&{ ;i}; [o;,	;["li;@�;i};-0;+To;
;	;
;[ ;["/* Active state */;@;i�o;;i ;[o;;i ;[o;;i ;["
color;[ ;o;	;"&nav_pills_active_link_hover_color;"&nav-pills-active-link-hover-color;@;i�;@;i�;;o;;i ;["background-color;[ ;o;	;"#nav_pills_active_link_hover_bg;"#nav-pills-active-link-hover-bg;@;i�;@;i�;;;@;["$&,
      &:hover,
      &:focus;i�;o;;" ;i�; [o;!; [o;"
;#0;@;$o;%;&{ ;i�; [o;';@;i�o;!; ["
o;"
;#0;@;$o;%;&{ ;i�; [o;';@;i�o;(
;["
hover;	;);@;i�;*0o;!; ["
o;"
;#0;@;$o;%;&{ ;i�; [o;';@;i�o;(
;["
focus;	;);@;i�;*0;+T;@;["&.active > a;i�;o;;" ;i�; [o;!; [o;"
;#0;@6;$o;%;&{ ;i�; [o;';@6;i�o;.;["active;@6;i�">o;"
;#0;@6;$o;%;&{ ;i�; [o;,	;["a;@6;i�;-0;+T;@;["	> li;i{;o;;" ;i{; [o;!; [">o;"
;#0;@M;$o;%;&{ ;i{; [o;,	;["li;@M;i{;-0;+T;@;[".nav-pills;iz;o;;" ;iz; [o;!; [o;"
;#0;@\;$o;%;&{ ;iz; [o;.;["nav-pills;@\;iz;+To;
;	;
;[ ;["/* Stacked pills */;@;i�o;;i ;[o;;i ;[o;;i ;["
float;[ ;o;;	;;"	none;@;@;i�;;o;;i ;[o;;i ;["margin-top;[ ;o;;	;;"2px;@;@;i�;;o;;i ;["margin-left;[ ;o;;	;;"0;@;@;i�;;o;
;	;
;[ ;["1/* no need for this gap between nav items */;@;i�;@;["	+ li;i�;o;;" ;i�; [o;!; ["+o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["li;@�;i�;-0;+T;@;["	> li;i�;o;;" ;i�; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["li;@�;i�;-0;+T;@;[".nav-stacked;i�;o;;" ;i�; [o;!; [o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["nav-stacked;@�;i�;+To;
;	;
;[ ;["O/* Nav variations
 * -------------------------------------------------- */;@;i�o;
;	;
;[ ;[";/* Justified nav links
 * ------------------------- */;@;i�o;;i ;[	o;;i ;["
width;[ ;o;;	;;"	100%;@;@;i�;;o;;i ;[o;;i ;["
float;[ ;o;;	;;"	none;@;@;i�;;o;;i ;[o;;i ;["text-align;[ ;o;;	;;"center;@;@;i�;;o;;i ;["margin-bottom;[ ;o;;	;;"5px;@;@;i�;;;@;["> a;i�;o;;" ;i�; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["a;@�;i�;-0;+T;@;["	> li;i�;o;;" ;i�; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["li;@�;i�;-0;+To;;i ;[o;;i ;["top;[ ;o;;	;;"	auto;@;@;i�;;o;;i ;["	left;[ ;o;;	;;"	auto;@;@;i�;;;@;["> .dropdown .dropdown-menu;i�;o;;" ;i�; [o;!; [">o;"
;#0;@;$o;%;&{ ;i�; [o;.;["dropdown;@;i�o;"
;#0;@;$o;%;&{ ;i�; [o;.;["dropdown-menu;@;i�;+To:Sass::Tree::MediaNode;i ;[o;;i ;[o;;i ;["display;[ ;o;;	;;"table-cell;@;@;i�;;o;;i ;["
width;[ ;o;;	;;"1%;@;@;i�;;o;;i ;[o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i�;;;@;["> a;i�;o;;" ;i�; [o;!; [">o;"
;#0;@6;$o;%;&{ ;i�; [o;,	;["a;@6;i�;-0;+T;@;["	> li;i�;o;;" ;i�; [o;!; [">o;"
;#0;@E;$o;%;&{ ;i�; [o;,	;["li;@E;i�;-0;+T;" ;@;i�;+T:@query[
"(o;	;	;;"min-width;@;i�": o;	;"screen_sm_min;"screen-sm-min;@;i�");@;[".nav-justified;i�;o;;" ;i�; [o;!; [o;"
;#0;@^;$o;%;&{ ;i�; [o;.;["nav-justified;@^;i�;+To;
;	;
;[ ;["�/* Move borders to anchors instead of bottom of list
 *
 * Mixin for adding on top the shared `.nav-justified` styles for our tabs */;@;i�o;;i ;[	o;;i ;["border-bottom;[ ;o;;	;;"0;@;@;i�;;o;;i ;[o;
;	;
;[ ;[")/* Override margin from .nav-tabs */;@;i�o;;i ;["margin-right;[ ;o;;	;;"0;@;@;i�;;o;;i ;["border-radius;[ ;o;	;"border_radius_base;"border-radius-base;@;i�;@;i�;;;@;["> li > a;i�;o;;" ;i�; [o;!; [	">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["li;@�;i�;-0">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["a;@�;i�;-0;+To;;i ;[o;;i ;["border;[ ;o;/	;[o;0;1["px;i;@;2"1px;i�;3[ o;	;	;;"
solid;@;i�o;	;")nav_tabs_justified_link_border_color;")nav-tabs-justified-link-border-color;@;i�;@;i�;4;5;@;i�;;;@;["@> .active > a,
  > .active > a:hover,
  > .active > a:focus;i�;o;;" ;i�; [o;!; [	">o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["active;@�;i�">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["a;@�;i�;-0o;!; [
"
">o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["active;@�;i�">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["a;@�;i�;-0o;(
;["
hover;	;);@�;i�;*0o;!; [
"
">o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["active;@�;i�">o;"
;#0;@�;$o;%;&{ ;i�; [o;,	;["a;@�;i�;-0o;(
;["
focus;	;);@�;i�;*0;+To;9;i ;[o;;i ;[o;;i ;["border-bottom;[ ;o;/	;[o;0;1["px;i;@;2"1px;i�;3[ o;	;	;;"
solid;@;i�o;	;")nav_tabs_justified_link_border_color;")nav-tabs-justified-link-border-color;@;i�;@;i�;4;5;@;i�;;o;;i ;["border-radius;[ ;o;/	;[	o;	;"border_radius_base;"border-radius-base;@;i�o;	;"border_radius_base;"border-radius-base;@;i�o;0;1[ ;i ;@;2"0;i�;3@�o;0;1[ ;i ;@;2"0;i�;3@�;@;i�;4;5;@;i�;;;@;["> li > a;i�;o;;" ;i�; [o;!; [	">o;"
;#0;@;$o;%;&{ ;i�; [o;,	;["li;@;i�;-0">o;"
;#0;@;$o;%;&{ ;i�; [o;,	;["a;@;i�;-0;+To;;i ;[o;;i ;["border-bottom-color;[ ;o;	;"0nav_tabs_justified_active_link_border_color;"0nav-tabs-justified-active-link-border-color;@;i�;@;i�;;;@;["D> .active > a,
    > .active > a:hover,
    > .active > a:focus;i�;o;;" ;i�; [o;!; [	">o;"
;#0;@=;$o;%;&{ ;i�; [o;.;["active;@=;i�">o;"
;#0;@=;$o;%;&{ ;i�; [o;,	;["a;@=;i�;-0o;!; [
"
">o;"
;#0;@=;$o;%;&{ ;i�; [o;.;["active;@=;i�">o;"
;#0;@=;$o;%;&{ ;i�; [o;,	;["a;@=;i�;-0o;(
;["
hover;	;);@=;i�;*0o;!; [
"
">o;"
;#0;@=;$o;%;&{ ;i�; [o;.;["active;@=;i�">o;"
;#0;@=;$o;%;&{ ;i�; [o;,	;["a;@=;i�;-0o;(
;["
focus;	;);@=;i�;*0;+T;" ;@;i�;+T;:[
"(o;	;	;;"min-width;@;i�": o;	;"screen_sm_min;"screen-sm-min;@;i�");@;[".nav-tabs-justified;i�;o;;" ;i�; [o;!; [o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["nav-tabs-justified;@�;i�;+To;
;	;
;[ ;["5/* Tabbable tabs
 * ------------------------- */;@;i�o;
;	;
;[ ;["A/* Hide tabbable panes to start, show them when `.active` */;@;i�o;;i ;[o;;i ;[o;;i ;["display;[ ;o;;	;;"	none;@;@;i�;;;@;["> .tab-pane;i�;o;;" ;i�; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["tab-pane;@�;i�;+To;;i ;[o;;i ;["display;[ ;o;;	;;"
block;@;@;i�;;;@;["> .active;i�;o;;" ;i�; [o;!; [">o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["active;@�;i�;+T;@;[".tab-content;i�;o;;" ;i�; [o;!; [o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["tab-content;@�;i�;+To;
;	;
;[ ;["1/* Dropdowns
 * ------------------------- */;@;i�o;
;	;
;[ ;["/* Specific dropdowns */;@;i�o;;i ;[	o;
;	;
;[ ;["2/* make dropdown border overlap tab border */;@;i�o;;i ;["margin-top;[ ;o;;	;;"	-1px;@;@;i�;;o;
;	;
;[ ;["X/* Remove the top rounded corners here since there is a hard edge above the menu */;@;i�o;;"border-top-radius;[ ;@;{ ;i�;0;[o;0;1[ ;i ;@;2"0;i�;3@�;@;[".nav-tabs .dropdown-menu;i�;o;;" ;i�; [o;!; [o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["nav-tabs;@�;i�o;"
;#0;@�;$o;%;&{ ;i�; [o;.;["dropdown-menu;@�;i�;+T;@;i;+T
3.2.10 (Media Mark)
9a989093c50c7f79743c2924c57ee71bb7121dd0
o:Sass::Tree::RootNode
:@template"�//
// Typography
// --------------------------------------------------


// Headings
// -------------------------

h1, h2, h3, h4, h5, h6,
.h1, .h2, .h3, .h4, .h5, .h6 {
  font-family: $headings-font-family;
  font-weight: $headings-font-weight;
  line-height: $headings-line-height;
  color: $headings-color;

  small,
  .small {
    font-weight: normal;
    line-height: 1;
    color: $headings-small-color;
  }
}

h1, .h1,
h2, .h2,
h3, .h3 {
  margin-top: $line-height-computed;
  margin-bottom: ($line-height-computed / 2);

  small,
  .small {
    font-size: 65%;
  }
}
h4, .h4,
h5, .h5,
h6, .h6 {
  margin-top: ($line-height-computed / 2);
  margin-bottom: ($line-height-computed / 2);

  small,
  .small {
    font-size: 75%;
  }
}

h1, .h1 { font-size: $font-size-h1; }
h2, .h2 { font-size: $font-size-h2; }
h3, .h3 { font-size: $font-size-h3; }
h4, .h4 { font-size: $font-size-h4; }
h5, .h5 { font-size: $font-size-h5; }
h6, .h6 { font-size: $font-size-h6; }


// Body text
// -------------------------

p {
  margin: 0 0 ($line-height-computed / 2);
}

.lead {
  margin-bottom: $line-height-computed;
  font-size: floor(($font-size-base * 1.15));
  font-weight: 200;
  line-height: 1.4;

  @media (min-width: $screen-sm-min) {
    font-size: ($font-size-base * 1.5);
  }
}


// Emphasis & misc
// -------------------------

// Ex: 14px base font * 85% = about 12px
small,
.small  { font-size: 85%; }

// Undo browser default styling
cite    { font-style: normal; }

// Alignment
.text-left           { text-align: left; }
.text-right          { text-align: right; }
.text-center         { text-align: center; }
.text-justify        { text-align: justify; }

// Contextual colors
.text-muted {
  color: $text-muted;
}

@include text-emphasis-variant('.text-primary', $brand-primary);

@include text-emphasis-variant('.text-success', $state-success-text);

@include text-emphasis-variant('.text-info', $state-info-text);

@include text-emphasis-variant('.text-warning', $state-warning-text);

@include text-emphasis-variant('.text-danger', $state-danger-text);

// Contextual backgrounds
// For now we'll leave these alongside the text classes until v4 when we can
// safely shift things around (per SemVer rules).
.bg-primary {
  // Given the contrast here, this is the only class to have its color inverted
  // automatically.
  color: #fff;
}
@include bg-variant('.bg-primary', $brand-primary);

@include bg-variant('.bg-success', $state-success-bg);

@include bg-variant('.bg-info', $state-info-bg);

@include bg-variant('.bg-warning', $state-warning-bg);

@include bg-variant('.bg-danger', $state-danger-bg);


// Page header
// -------------------------

.page-header {
  padding-bottom: (($line-height-computed / 2) - 1);
  margin: ($line-height-computed * 2) 0 $line-height-computed;
  border-bottom: 1px solid $page-header-border-color;
}


// Lists
// --------------------------------------------------

// Unordered and Ordered lists
ul,
ol {
  margin-top: 0;
  margin-bottom: ($line-height-computed / 2);
  ul,
  ol {
    margin-bottom: 0;
  }
}

// List options

// Unstyled keeps list items block level, just removes default browser padding and list-style
.list-unstyled {
  padding-left: 0;
  list-style: none;
}

// Inline turns list items into inline-block
.list-inline {
  @extend .list-unstyled;
  margin-left: -5px;

  > li {
    display: inline-block;
    padding-left: 5px;
    padding-right: 5px;
  }
}

// Description Lists
dl {
  margin-top: 0; // Remove browser default
  margin-bottom: $line-height-computed;
}
dt,
dd {
  line-height: $line-height-base;
}
dt {
  font-weight: bold;
}
dd {
  margin-left: 0; // Undo browser default
}

// Horizontal description lists
//
// Defaults to being stacked without any of the below styles applied, until the
// grid breakpoint is reached (default of ~768px).

@media (min-width: $grid-float-breakpoint) {
  .dl-horizontal {
    dt {
      float: left;
      width: ($component-offset-horizontal - 20);
      clear: left;
      text-align: right;
      @include text-overflow();
    }
    dd {
      margin-left: $component-offset-horizontal;
      @include clearfix(); // Clear the floated `dt` if an empty `dd` is present
    }
  }
}

// MISC
// ----

// Abbreviations and acronyms
abbr[title],
// Add data-* attribute to help out our tooltip plugin, per https://github.com/twbs/bootstrap/issues/5257
abbr[data-original-title] {
  cursor: help;
  border-bottom: 1px dotted $abbr-border-color;
}
.initialism {
  font-size: 90%;
  text-transform: uppercase;
}

// Blockquotes
blockquote {
  padding: ($line-height-computed / 2) $line-height-computed;
  margin: 0 0 $line-height-computed;
  font-size: $blockquote-font-size;
  border-left: 5px solid $blockquote-border-color;

  p,
  ul,
  ol {
    &:last-child {
      margin-bottom: 0;
    }
  }

  // Note: Deprecated small and .small as of v3.1.0
  // Context: https://github.com/twbs/bootstrap/issues/11660
  footer,
  small,
  .small {
    display: block;
    font-size: 80%; // back to default font-size
    line-height: $line-height-base;
    color: $blockquote-small-color;

    &:before {
      content: '\2014 \00A0'; // em dash, nbsp
    }
  }
}

// Opposite alignment of blockquote
//
// Heads up: `blockquote.pull-right` has been deprecated as of v3.1.0.
.blockquote-reverse,
blockquote.pull-right {
  padding-right: 15px;
  padding-left: 0;
  border-right: 5px solid $blockquote-border-color;
  border-left: 0;
  text-align: right;

  // Account for citation
  footer,
  small,
  .small {
    &:before { content: ''; }
    &:after {
      content: '\00A0 \2014'; // nbsp, em dash
    }
  }
}

// Quotes
blockquote:before,
blockquote:after {
  content: "";
}

// Addresses
address {
  margin-bottom: $line-height-computed;
  font-style: normal;
  line-height: $line-height-base;
}
:@children[Ho:Sass::Tree::CommentNode
:
@type:silent;[ :@value["N/*
 * Typography
 * -------------------------------------------------- */:@options{ :
@lineio;
;	;
;[ ;["0/* Headings
 * ------------------------- */;@;io:Sass::Tree::RuleNode:
@tabsi ;[
o:Sass::Tree::PropNode;i :
@name["font-family;[ ;o:Sass::Script::Variable	:@underscored_name"headings_font_family;"headings-font-family;@;i;@;i:@prop_syntax:newo;;i ;["font-weight;[ ;o;	;"headings_font_weight;"headings-font-weight;@;i;@;i;;o;;i ;["line-height;[ ;o;	;"headings_line_height;"headings-line-height;@;i;@;i;;o;;i ;["
color;[ ;o;	;"headings_color;"headings-color;@;i;@;i;;o;;i ;[o;;i ;["font-weight;[ ;o:Sass::Script::String;	:identifier;"normal;@;@;i;;o;;i ;["line-height;[ ;o;;	;;"1;@;@;i;;o;;i ;["
color;[ ;o;	;"headings_small_color;"headings-small-color;@;i;@;i;;;@:
@rule["small,
  .small;i:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence
:@subject0;@G:@sourceso:Set:
@hash{ ;i;[o:Sass::Selector::Element	;["
small;@G;i:@namespace0o;;["
o;
;0;@G; o;!;"{ ;i;[o:Sass::Selector::Class;["
small;@G;i:@has_childrenT;@;["9h1, h2, h3, h4, h5, h6,
.h1, .h2, .h3, .h4, .h5, .h6;i;o;;" ;i;[o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h1;@_;i;$0o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h2;@_;i;$0o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h3;@_;i;$0o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h4;@_;i;$0o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h5;@_;i;$0o;;[o;
;0;@_; o;!;"{ ;i;[o;#	;["h6;@_;i;$0o;;["
o;
;0;@_; o;!;"{ ;i;[o;%;["h1;@_;io;;[o;
;0;@_; o;!;"{ ;i;[o;%;["h2;@_;io;;[o;
;0;@_; o;!;"{ ;i;[o;%;["h3;@_;io;;[o;
;0;@_; o;!;"{ ;i;[o;%;["h4;@_;io;;[o;
;0;@_; o;!;"{ ;i;[o;%;["h5;@_;io;;[o;
;0;@_; o;!;"{ ;i;[o;%;["h6;@_;i;&To;;i ;[o;;i ;["margin-top;[ ;o;	;"line_height_computed;"line-height-computed;@;i ;@;i ;;o;;i ;["margin-bottom;[ ;o:Sass::Script::Operation
;@:@operand2o:Sass::Script::Number:@numerator_units[ ;i;@:@original"2;i!:@denominator_units[ ;i!:@operator:div:@operand1o;	;"line_height_computed;"line-height-computed;@;i!;@;i!;;o;;i ;[o;;i ;["font-size;[ ;o;;	;;"65%;@;@;i%;;;@;["small,
  .small;i$;o;;" ;i$;[o;;[o;
;0;@�; o;!;"{ ;i$;[o;#	;["
small;@�;i$;$0o;;["
o;
;0;@�; o;!;"{ ;i$;[o;%;["
small;@�;i$;&T;@;["h1, .h1,
h2, .h2,
h3, .h3;i;o;;" ;i;[o;;[o;
;0;@; o;!;"{ ;i;[o;#	;["h1;@;i;$0o;;[o;
;0;@; o;!;"{ ;i;[o;%;["h1;@;io;;["
o;
;0;@; o;!;"{ ;i;[o;#	;["h2;@;i;$0o;;[o;
;0;@; o;!;"{ ;i;[o;%;["h2;@;io;;["
o;
;0;@; o;!;"{ ;i;[o;#	;["h3;@;i;$0o;;[o;
;0;@; o;!;"{ ;i;[o;%;["h3;@;i;&To;;i ;[o;;i ;["margin-top;[ ;o;'
;@;(o;);*[ ;i;@;+"2;i+;,@�;i+;-;.;/o;	;"line_height_computed;"line-height-computed;@;i+;@;i+;;o;;i ;["margin-bottom;[ ;o;'
;@;(o;);*[ ;i;@;+"2;i,;,@�;i,;-;.;/o;	;"line_height_computed;"line-height-computed;@;i,;@;i,;;o;;i ;[o;;i ;["font-size;[ ;o;;	;;"75%;@;@;i0;;;@;["small,
  .small;i/;o;;" ;i/;[o;;[o;
;0;@^; o;!;"{ ;i/;[o;#	;["
small;@^;i/;$0o;;["
o;
;0;@^; o;!;"{ ;i/;[o;%;["
small;@^;i/;&T;@;["h4, .h4,
h5, .h5,
h6, .h6;i*;o;;" ;i*;[o;;[o;
;0;@v; o;!;"{ ;i*;[o;#	;["h4;@v;i*;$0o;;[o;
;0;@v; o;!;"{ ;i*;[o;%;["h4;@v;i*o;;["
o;
;0;@v; o;!;"{ ;i*;[o;#	;["h5;@v;i*;$0o;;[o;
;0;@v; o;!;"{ ;i*;[o;%;["h5;@v;i*o;;["
o;
;0;@v; o;!;"{ ;i*;[o;#	;["h6;@v;i*;$0o;;[o;
;0;@v; o;!;"{ ;i*;[o;%;["h6;@v;i*;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h1;"font-size-h1;@;i4;@;i4;;;@;["h1, .h1;i4;o;;" ;i4;[o;;[o;
;0;@�; o;!;"{ ;i4;[o;#	;["h1;@�;i4;$0o;;[o;
;0;@�; o;!;"{ ;i4;[o;%;["h1;@�;i4;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h2;"font-size-h2;@;i5;@;i5;;;@;["h2, .h2;i5;o;;" ;i5;[o;;[o;
;0;@�; o;!;"{ ;i5;[o;#	;["h2;@�;i5;$0o;;[o;
;0;@�; o;!;"{ ;i5;[o;%;["h2;@�;i5;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h3;"font-size-h3;@;i6;@;i6;;;@;["h3, .h3;i6;o;;" ;i6;[o;;[o;
;0;@�; o;!;"{ ;i6;[o;#	;["h3;@�;i6;$0o;;[o;
;0;@�; o;!;"{ ;i6;[o;%;["h3;@�;i6;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h4;"font-size-h4;@;i7;@;i7;;;@;["h4, .h4;i7;o;;" ;i7;[o;;[o;
;0;@; o;!;"{ ;i7;[o;#	;["h4;@;i7;$0o;;[o;
;0;@; o;!;"{ ;i7;[o;%;["h4;@;i7;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h5;"font-size-h5;@;i8;@;i8;;;@;["h5, .h5;i8;o;;" ;i8;[o;;[o;
;0;@<; o;!;"{ ;i8;[o;#	;["h5;@<;i8;$0o;;[o;
;0;@<; o;!;"{ ;i8;[o;%;["h5;@<;i8;&To;;i ;[o;;i ;["font-size;[ ;o;	;"font_size_h6;"font-size-h6;@;i9;@;i9;;;@;["h6, .h6;i9;o;;" ;i9;[o;;[o;
;0;@\; o;!;"{ ;i9;[o;#	;["h6;@\;i9;$0o;;[o;
;0;@\; o;!;"{ ;i9;[o;%;["h6;@\;i9;&To;
;	;
;[ ;["1/* Body text
 * ------------------------- */;@;i<o;;i ;[o;;i ;["margin;[ ;o:Sass::Script::List	;[o;);*[ ;i ;@;+"0;i@;,@�o;);*[ ;i ;@;+"0;i@;,@�o;'
;@;(o;);*[ ;i;@;+"2;i@;,@�;i@;-;.;/o;	;"line_height_computed;"line-height-computed;@;i@;@;i@:@separator:
space;@;i@;;;@;["p;i?;o;;" ;i?;[o;;[o;
;0;@�; o;!;"{ ;i?;[o;#	;["p;@�;i?;$0;&To;;i ;[
o;;i ;["margin-bottom;[ ;o;	;"line_height_computed;"line-height-computed;@;iD;@;iD;;o;;i ;["font-size;[ ;o:Sass::Script::Funcall;"
floor;@:@keywords{ ;iE:@splat0:
@args[o;'
;@;(o;);*[ ;f1.1499999999999999 ff;@;+"	1.15;iE;,@�;iE;-:
times;/o;	;"font_size_base;"font-size-base;@;iE;@;iE;;o;;i ;["font-weight;[ ;o;;	;;"200;@;@;iF;;o;;i ;["line-height;[ ;o;;	;;"1.4;@;@;iG;;o:Sass::Tree::MediaNode;i ;[o;;i ;["font-size;[ ;o;'
;@;(o;);*[ ;f1.5;@;+"1.5;iJ;,@�;iJ;-;7;/o;	;"font_size_base;"font-size-base;@;iJ;@;iJ;;;" ;@;iI;&T:@query[
"(o;	;	;;"min-width;@;iI": o;	;"screen_sm_min;"screen-sm-min;@;iI");@;["
.lead;iC;o;;" ;iC;[o;;[o;
;0;@�; o;!;"{ ;iC;[o;%;["	lead;@�;iC;&To;
;	;
;[ ;["7/* Emphasis & misc
 * ------------------------- */;@;iOo;
;	;
;[ ;["0/* Ex: 14px base font * 85% = about 12px */;@;iRo;;i ;[o;;i ;["font-size;[ ;o;;	;;"85%;@;@;iT;;;@;["small,
.small;iT;o;;" ;iT;[o;;[o;
;0;@�; o;!;"{ ;iT;[o;#	;["
small;@�;iT;$0o;;["
o;
;0;@�; o;!;"{ ;iT;[o;%;["
small;@�;iT;&To;
;	;
;[ ;["'/* Undo browser default styling */;@;iVo;;i ;[o;;i ;["font-style;[ ;o;;	;;"normal;@;@;iW;;;@;["	cite;iW;o;;" ;iW;[o;;[o;
;0;@; o;!;"{ ;iW;[o;#	;["	cite;@;iW;$0;&To;
;	;
;[ ;["/* Alignment */;@;iYo;;i ;[o;;i ;["text-align;[ ;o;;	;;"	left;@;@;iZ;;;@;[".text-left;iZ;o;;" ;iZ;[o;;[o;
;0;@3; o;!;"{ ;iZ;[o;%;["text-left;@3;iZ;&To;;i ;[o;;i ;["text-align;[ ;o;;	;;"
right;@;@;i[;;;@;[".text-right;i[;o;;" ;i[;[o;;[o;
;0;@I; o;!;"{ ;i[;[o;%;["text-right;@I;i[;&To;;i ;[o;;i ;["text-align;[ ;o;;	;;"center;@;@;i\;;;@;[".text-center;i\;o;;" ;i\;[o;;[o;
;0;@_; o;!;"{ ;i\;[o;%;["text-center;@_;i\;&To;;i ;[o;;i ;["text-align;[ ;o;;	;;"justify;@;@;i];;;@;[".text-justify;i];o;;" ;i];[o;;[o;
;0;@u; o;!;"{ ;i];[o;%;["text-justify;@u;i];&To;
;	;
;[ ;["/* Contextual colors */;@;i_o;;i ;[o;;i ;["
color;[ ;o;	;"text_muted;"text-muted;@;ia;@;ia;;;@;[".text-muted;i`;o;;" ;i`;[o;;[o;
;0;@�; o;!;"{ ;i`;[o;%;["text-muted;@�;i`;&To:Sass::Tree::MixinNode;"text-emphasis-variant;[ ;@;4{ ;id;50;6[o;	;	:string;".text-primary;@;ido;	;"brand_primary;"brand-primary;@;ido;:;"text-emphasis-variant;[ ;@;4{ ;if;50;6[o;	;	;;;".text-success;@;ifo;	;"state_success_text;"state-success-text;@;ifo;:;"text-emphasis-variant;[ ;@;4{ ;ih;50;6[o;	;	;;;".text-info;@;iho;	;"state_info_text;"state-info-text;@;iho;:;"text-emphasis-variant;[ ;@;4{ ;ij;50;6[o;	;	;;;".text-warning;@;ijo;	;"state_warning_text;"state-warning-text;@;ijo;:;"text-emphasis-variant;[ ;@;4{ ;il;50;6[o;	;	;;;".text-danger;@;ilo;	;"state_danger_text;"state-danger-text;@;ilo;
;	;
;[ ;["�/* Contextual backgrounds
 * For now we'll leave these alongside the text classes until v4 when we can
 * safely shift things around (per SemVer rules). */;@;ino;;i ;[o;
;	;
;[ ;["g/* Given the contrast here, this is the only class to have its color inverted
 * automatically. */;@;iro;;i ;["
color;[ ;o;;	;;"	#fff;@;@;it;;;@;[".bg-primary;iq;o;;" ;iq;[o;;[o;
;0;@�; o;!;"{ ;iq;[o;%;["bg-primary;@�;iq;&To;:;"bg-variant;[ ;@;4{ ;iv;50;6[o;	;	;;;".bg-primary;@;ivo;	;"brand_primary;"brand-primary;@;ivo;:;"bg-variant;[ ;@;4{ ;ix;50;6[o;	;	;;;".bg-success;@;ixo;	;"state_success_bg;"state-success-bg;@;ixo;:;"bg-variant;[ ;@;4{ ;iz;50;6[o;	;	;;;".bg-info;@;izo;	;"state_info_bg;"state-info-bg;@;izo;:;"bg-variant;[ ;@;4{ ;i|;50;6[o;	;	;;;".bg-warning;@;i|o;	;"state_warning_bg;"state-warning-bg;@;i|o;:;"bg-variant;[ ;@;4{ ;i~;50;6[o;	;	;;;".bg-danger;@;i~o;	;"state_danger_bg;"state-danger-bg;@;i~o;
;	;
;[ ;["3/* Page header
 * ------------------------- */;@;i|o;;i ;[o;;i ;["padding-bottom;[ ;o;'
;@;(o;)
;*[ ;i;@;i�;,@�;i�;-:
minus;/o;'
;@;(o;);*[ ;i;@;+"2;i�;,@�;i�;-;.;/o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;;o;;i ;["margin;[ ;o;0	;[o;'
;@;(o;);*[ ;i;@;+"2;i�;,@�;i�;-;7;/o;	;"line_height_computed;"line-height-computed;@;i�o;);*[ ;i ;@;+"0;i�;,@�o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;1;2;@;i�;;o;;i ;["border-bottom;[ ;o;0	;[o;);*["px;i;@;+"1px;i�;,[ o;	;	;;"
solid;@;i�o;	;"page_header_border_color;"page-header-border-color;@;i�;@;i�;1;2;@;i�;;;@;[".page-header;i;o;;" ;i;[o;;[o;
;0;@W; o;!;"{ ;i;[o;%;["page-header;@W;i;&To;
;	;
;[ ;["F/* Lists
 * -------------------------------------------------- */;@;i�o;
;	;
;[ ;["&/* Unordered and Ordered lists */;@;i�o;;i ;[o;;i ;["margin-top;[ ;o;;	;;"0;@;@;i�;;o;;i ;["margin-bottom;[ ;o;'
;@;(o;);*[ ;i;@;+"2;i�;,@�;i�;-;.;/o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;;o;;i ;[o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i�;;;@;["ul,
  ol;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["ul;@�;i�;$0o;;["
o;
;0;@�; o;!;"{ ;i�;[o;#	;["ol;@�;i�;$0;&T;@;["ul,
ol;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["ul;@�;i�;$0o;;["
o;
;0;@�; o;!;"{ ;i�;[o;#	;["ol;@�;i�;$0;&To;
;	;
;[ ;["/* List options */;@;i�o;
;	;
;[ ;["e/* Unstyled keeps list items block level, just removes default browser padding and list-style */;@;i�o;;i ;[o;;i ;["padding-left;[ ;o;;	;;"0;@;@;i�;;o;;i ;["list-style;[ ;o;;	;;"	none;@;@;i�;;;@;[".list-unstyled;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;%;["list-unstyled;@�;i�;&To;
;	;
;[ ;["4/* Inline turns list items into inline-block */;@;i�o;;i ;[o:Sass::Tree::ExtendNode
;[ ;@:@selector["."list-unstyled;i�:@optionalFo;;i ;["margin-left;[ ;o;;	;;"	-5px;@;@;i�;;o;;i ;[o;;i ;["display;[ ;o;;	;;"inline-block;@;@;i�;;o;;i ;["padding-left;[ ;o;;	;;"5px;@;@;i�;;o;;i ;["padding-right;[ ;o;;	;;"5px;@;@;i�;;;@;["	> li;i�;o;;" ;i�;[o;;[">o;
;0;@; o;!;"{ ;i�;[o;#	;["li;@;i�;$0;&T;@;[".list-inline;i�;o;;" ;i�;[o;;[o;
;0;@; o;!;"{ ;i�;[o;%;["list-inline;@;i�;&To;
;	;
;[ ;["/* Description Lists */;@;i�o;;i ;[o;;i ;["margin-top;[ ;o;;	;;"0;@;@;i�;;o;
;	;
;[ ;["!/* Remove browser default */;@;i�o;;i ;["margin-bottom;[ ;o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;;;@;["dl;i�;o;;" ;i�;[o;;[o;
;0;@5; o;!;"{ ;i�;[o;#	;["dl;@5;i�;$0;&To;;i ;[o;;i ;["line-height;[ ;o;	;"line_height_base;"line-height-base;@;i�;@;i�;;;@;["dt,
dd;i�;o;;" ;i�;[o;;[o;
;0;@L; o;!;"{ ;i�;[o;#	;["dt;@L;i�;$0o;;["
o;
;0;@L; o;!;"{ ;i�;[o;#	;["dd;@L;i�;$0;&To;;i ;[o;;i ;["font-weight;[ ;o;;	;;"	bold;@;@;i�;;;@;["dt;i�;o;;" ;i�;[o;;[o;
;0;@l; o;!;"{ ;i�;[o;#	;["dt;@l;i�;$0;&To;;i ;[o;;i ;["margin-left;[ ;o;;	;;"0;@;@;i�;;o;
;	;
;[ ;["/* Undo browser default */;@;i�;@;["dd;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["dd;@�;i�;$0;&To;
;	;
;[ ;["�/* Horizontal description lists
 *
 * Defaults to being stacked without any of the below styles applied, until the
 * grid breakpoint is reached (default of ~768px). */;@;i�o;8;i ;[o;;i ;[o;;i ;[
o;;i ;["
float;[ ;o;;	;;"	left;@;@;i�;;o;;i ;["
width;[ ;o;'
;@;(o;);*[ ;i;@;+"20;i�;,@�;i�;-;<;/o;	;" component_offset_horizontal;" component-offset-horizontal;@;i�;@;i�;;o;;i ;["
clear;[ ;o;;	;;"	left;@;@;i�;;o;;i ;["text-align;[ ;o;;	;;"
right;@;@;i�;;o;:;"text-overflow;[ ;@;4{ ;i�;50;6[ ;@;["dt;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["dt;@�;i�;$0;&To;;i ;[o;;i ;["margin-left;[ ;o;	;" component_offset_horizontal;" component-offset-horizontal;@;i�;@;i�;;o;:;"clearfix;[ ;@;4{ ;i�;50;6[ o;
;	;
;[ ;["=/* Clear the floated `dt` if an empty `dd` is present */;@;i�;@;["dd;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["dd;@�;i�;$0;&T;@;[".dl-horizontal;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;%;["dl-horizontal;@�;i�;&T;" ;@;i�;&T;9[
"(o;	;	;;"min-width;@;i�": o;	;"grid_float_breakpoint;"grid-float-breakpoint;@;i�")o;
;	;
;[ ;["/* MISC
 * ---- */;@;i�o;
;	;
;[ ;["%/* Abbreviations and acronyms */;@;i�o;;i ;[o;;i ;["cursor;[ ;o;;	;;"	help;@;@;i�;;o;;i ;["border-bottom;[ ;o;0	;[o;);*["px;i;@;+"1px;i�;,[ o;	;	;;"dotted;@;i�o;	;"abbr_border_color;"abbr-border-color;@;i�;@;i�;1;2;@;i�;;;@;[",abbr[title],

abbr[data-original-title];i�;o;;" ;i�;[o;;[o;
;0;@&; o;!;"{ ;i�;[o;#	;["	abbr;@&;i�;$0o:Sass::Selector::Attribute;["
title;0;@&:@flags0;i�;$0;-0o;;["
o;
;0;@&; o;!;"{ ;i�;[o;#	;["	abbr;@&;i�;$0o;@;["data-original-title;0;@&;A0;i�;$0;-0;&To;;i ;[o;;i ;["font-size;[ ;o;;	;;"90%;@;@;i�;;o;;i ;["text-transform;[ ;o;;	;;"uppercase;@;@;i�;;;@;[".initialism;i�;o;;" ;i�;[o;;[o;
;0;@R; o;!;"{ ;i�;[o;%;["initialism;@R;i�;&To;
;	;
;[ ;["/* Blockquotes */;@;i�o;;i ;[o;;i ;["padding;[ ;o;0	;[o;'
;@;(o;);*[ ;i;@;+"2;i�;,@�;i�;-;.;/o;	;"line_height_computed;"line-height-computed;@;i�o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;1;2;@;i�;;o;;i ;["margin;[ ;o;0	;[o;);*[ ;i ;@;+"0;i�;,@�o;);*[ ;i ;@;+"0;i�;,@�o;	;"line_height_computed;"line-height-computed;@;i�;@;i�;1;2;@;i�;;o;;i ;["font-size;[ ;o;	;"blockquote_font_size;"blockquote-font-size;@;i�;@;i�;;o;;i ;["border-left;[ ;o;0	;[o;);*["px;i
;@;+"5px;i�;,[ o;	;	;;"
solid;@;i�o;	;"blockquote_border_color;"blockquote-border-color;@;i�;@;i�;1;2;@;i�;;o;;i ;[o;;i ;[o;;i ;["margin-bottom;[ ;o;;	;;"0;@;@;i�;;;@;["&:last-child;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o:Sass::Selector::Parent;@�;i�o:Sass::Selector::Pseudo
;["last-child;	:
class;@�;i�:	@arg0;&T;@;["p,
  ul,
  ol;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;#	;["p;@�;i�;$0o;;["
o;
;0;@�; o;!;"{ ;i�;[o;#	;["ul;@�;i�;$0o;;["
o;
;0;@�; o;!;"{ ;i�;[o;#	;["ol;@�;i�;$0;&To;
;	;
;[ ;["t/* Note: Deprecated small and .small as of v3.1.0
 * Context: https://github.com/twbs/bootstrap/issues/11660 */;@;i�o;;i ;[o;;i ;["display;[ ;o;;	;;"
block;@;@;i�;;o;;i ;["font-size;[ ;o;;	;;"80%;@;@;i�;;o;
;	;
;[ ;["$/* back to default font-size */;@;i�o;;i ;["line-height;[ ;o;	;"line_height_base;"line-height-base;@;i�;@;i�;;o;;i ;["
color;[ ;o;	;"blockquote_small_color;"blockquote-small-color;@;i�;@;i�;;o;;i ;[o;;i ;["content;[ ;o;;	;;"'\2014 \00A0';@;@;i�;;o;
;	;
;[ ;["/* em dash, nbsp */;@;i�;@;["&:before;i�;o;;" ;i�;[o;;[o;
;0;@; o;!;"{ ;i�;[o;B;@;i�o;C
;["before;	;D;@;i�;E0;&T;@;["footer,
  small,
  .small;i�;o;;" ;i�;[o;;[o;
;0;@; o;!;"{ ;i�;[o;#	;["footer;@;i�;$0o;;["
o;
;0;@; o;!;"{ ;i�;[o;#	;["
small;@;i�;$0o;;["
o;
;0;@; o;!;"{ ;i�;[o;%;["
small;@;i�;&T;@;["blockquote;i�;o;;" ;i�;[o;;[o;
;0;@8; o;!;"{ ;i�;[o;#	;["blockquote;@8;i�;$0;&To;
;	;
;[ ;["u/* Opposite alignment of blockquote
 *
 * Heads up: `blockquote.pull-right` has been deprecated as of v3.1.0. */;@;i�o;;i ;[o;;i ;["padding-right;[ ;o;;	;;"	15px;@;@;i ;;o;;i ;["padding-left;[ ;o;;	;;"0;@;@;i;;o;;i ;["border-right;[ ;o;0	;[o;);*["px;i
;@;+"5px;i;,[ o;	;	;;"
solid;@;io;	;"blockquote_border_color;"blockquote-border-color;@;i;@;i;1;2;@;i;;o;;i ;["border-left;[ ;o;;	;;"0;@;@;i;;o;;i ;["text-align;[ ;o;;	;;"
right;@;@;i;;o;
;	;
;[ ;["/* Account for citation */;@;io;;i ;[o;;i ;[o;;i ;["content;[ ;o;;	;;"'';@;@;i
;;;@;["&:before;i
;o;;" ;i
;[o;;[o;
;0;@�; o;!;"{ ;i
;[o;B;@�;i
o;C
;["before;	;D;@�;i
;E0;&To;;i ;[o;;i ;["content;[ ;o;;	;;"'\00A0 \2014';@;@;i;;o;
;	;
;[ ;["/* nbsp, em dash */;@;i;@;["&:after;i;o;;" ;i;[o;;[o;
;0;@�; o;!;"{ ;i;[o;B;@�;io;C
;["
after;	;D;@�;i;E0;&T;@;["footer,
  small,
  .small;i	;o;;" ;i	;[o;;[o;
;0;@�; o;!;"{ ;i	;[o;#	;["footer;@�;i	;$0o;;["
o;
;0;@�; o;!;"{ ;i	;[o;#	;["
small;@�;i	;$0o;;["
o;
;0;@�; o;!;"{ ;i	;[o;%;["
small;@�;i	;&T;@;["/.blockquote-reverse,
blockquote.pull-right;i�;o;;" ;i�;[o;;[o;
;0;@�; o;!;"{ ;i�;[o;%;["blockquote-reverse;@�;i�o;;["
o;
;0;@�; o;!;"{ ;i�;[o;#	;["blockquote;@�;i�;$0o;%;["pull-right;@�;i�;&To;
;	;
;[ ;["/* Quotes */;@;io;;i ;[o;;i ;["content;[ ;o;;	;;""";@;@;i;;;@;["(blockquote:before,
blockquote:after;i;o;;" ;i;[o;;[o;
;0;@�; o;!;"{ ;i;[o;#	;["blockquote;@�;i;$0o;C
;["before;	;D;@�;i;E0o;;["
o;
;0;@�; o;!;"{ ;i;[o;#	;["blockquote;@�;i;$0o;C
;["
after;	;D;@�;i;E0;&To;
;	;
;[ ;["/* Addresses */;@;io;;i ;[o;;i ;["margin-bottom;[ ;o;	;"line_height_computed;"line-height-computed;@;i;@;i;;o;;i ;["font-style;[ ;o;;	;;"normal;@;@;i;;o;;i ;["line-height;[ ;o;	;"line_height_base;"line-height-base;@;i;@;i;;;@;["address;i;o;;" ;i;[o;;[o;
;0;@-; o;!;"{ ;i;[o;#	;["address;@-;i;$0;&T;@;i;&T